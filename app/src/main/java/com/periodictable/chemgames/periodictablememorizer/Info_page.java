package com.periodictable.chemgames.periodictablememorizer;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.view.View.INVISIBLE;
import static com.periodictable.chemgames.periodictablememorizer.R.color.*;


public class Info_page extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.ll_box1)  LinearLayout box1;
    @BindView(R.id.ll_box2)  LinearLayout box2;
    @BindView(R.id.ll_box3)  LinearLayout box3;
    @BindView(R.id.ll_box4)  LinearLayout box4;
    @BindView(R.id.ll_box5)  LinearLayout box5;
    @BindView(R.id.ll_box6)  LinearLayout box6;
    @BindView(R.id.ll_box7)  LinearLayout box7;
    @BindView(R.id.ll_box8)  LinearLayout box8;
    @BindView(R.id.ll_box9)  LinearLayout box9;
    @BindView(R.id.ll_box10)  LinearLayout box10;
    @BindView(R.id.ll_box11)  LinearLayout box11;
    @BindView(R.id.ll_box12)  LinearLayout box12;
    @BindView(R.id.ll_box13)  LinearLayout box13;
    @BindView(R.id.ll_box14)  LinearLayout box14;
    @BindView(R.id.ll_box15)  LinearLayout box15;
    @BindView(R.id.ll_box16)  LinearLayout box16;
    @BindView(R.id.ll_box17)  LinearLayout box17;
    @BindView(R.id.ll_box18)  LinearLayout box18;
    @BindView(R.id.ll_box19)  LinearLayout box19;
    @BindView(R.id.ll_box20)  LinearLayout box20;
    @BindView(R.id.ll_box21)  LinearLayout box21;
    @BindView(R.id.ll_box22)  LinearLayout box22;
    @BindView(R.id.ll_box23)  LinearLayout box23;
    @BindView(R.id.ll_box24)  LinearLayout box24;
    @BindView(R.id.ll_box25)  LinearLayout box25;
    @BindView(R.id.ll_box26)  LinearLayout box26;
    @BindView(R.id.ll_box27)  LinearLayout box27;
    @BindView(R.id.ll_box28)  LinearLayout box28;
    @BindView(R.id.ll_box29)  LinearLayout box29;
    @BindView(R.id.ll_box30)  LinearLayout box30;
    @BindView(R.id.ll_box31)  LinearLayout box31;
    @BindView(R.id.ll_box32)  LinearLayout box32;
    @BindView(R.id.ll_box33)  LinearLayout box33;
    @BindView(R.id.ll_box34)  LinearLayout box34;
    @BindView(R.id.ll_box35)  LinearLayout box35;
    @BindView(R.id.ll_box36)  LinearLayout box36;
    @BindView(R.id.ll_box37)  LinearLayout box37;
    @BindView(R.id.ll_box38)  LinearLayout box38;
    @BindView(R.id.ll_box39)  LinearLayout box39;
    @BindView(R.id.ll_box40)  LinearLayout box40;
    @BindView(R.id.ll_box41)  LinearLayout box41;
    @BindView(R.id.ll_box42)  LinearLayout box42;
    @BindView(R.id.ll_box43)  LinearLayout box43;
    @BindView(R.id.ll_box44)  LinearLayout box44;
    @BindView(R.id.ll_box45)  LinearLayout box45;
    @BindView(R.id.ll_box46)  LinearLayout box46;
    @BindView(R.id.ll_box47)  LinearLayout box47;
    @BindView(R.id.ll_box48)  LinearLayout box48;
    @BindView(R.id.ll_box49)  LinearLayout box49;
    @BindView(R.id.ll_box50)  LinearLayout box50;
    @BindView(R.id.ll_box51)  LinearLayout box51;
    @BindView(R.id.ll_box52)  LinearLayout box52;
    @BindView(R.id.ll_box53)  LinearLayout box53;
    @BindView(R.id.ll_box54)  LinearLayout box54;
    @BindView(R.id.ll_box55)  LinearLayout box55;
    @BindView(R.id.ll_box56)  LinearLayout box56;
    @BindView(R.id.ll_box57) LinearLayout box57;
    @BindView(R.id.ll_box58) LinearLayout box58;
    @BindView(R.id.ll_box59) LinearLayout box59;
    @BindView(R.id.ll_box60) LinearLayout box60;
    @BindView(R.id.ll_box61) LinearLayout box61;
    @BindView(R.id.ll_box62) LinearLayout box62;
    @BindView(R.id.ll_box63) LinearLayout box63;
    @BindView(R.id.ll_box64) LinearLayout box64;
    @BindView(R.id.ll_box65) LinearLayout box65;
    @BindView(R.id.ll_box66) LinearLayout box66;
    @BindView(R.id.ll_box67) LinearLayout box67;
    @BindView(R.id.ll_box68) LinearLayout box68;
    @BindView(R.id.ll_box69) LinearLayout box69;
    @BindView(R.id.ll_box70) LinearLayout box70;
    @BindView(R.id.ll_box71) LinearLayout box71;
    @BindView(R.id.ll_box72)  LinearLayout box72;
    @BindView(R.id.ll_box73)  LinearLayout box73;
    @BindView(R.id.ll_box74)  LinearLayout box74;
    @BindView(R.id.ll_box75)  LinearLayout box75;
    @BindView(R.id.ll_box76)  LinearLayout box76;
    @BindView(R.id.ll_box77)  LinearLayout box77;
    @BindView(R.id.ll_box78)  LinearLayout box78;
    @BindView(R.id.ll_box79)  LinearLayout box79;
    @BindView(R.id.ll_box80)  LinearLayout box80;
    @BindView(R.id.ll_box81)  LinearLayout box81;
    @BindView(R.id.ll_box82)  LinearLayout box82;
    @BindView(R.id.ll_box83)  LinearLayout box83;
    @BindView(R.id.ll_box84)  LinearLayout box84;
    @BindView(R.id.ll_box85)  LinearLayout box85;
    @BindView(R.id.ll_box86)  LinearLayout box86;
    @BindView(R.id.ll_box87)  LinearLayout box87;
    @BindView(R.id.ll_box88)  LinearLayout box88;
    @BindView(R.id.ll_box89) LinearLayout box89;
    @BindView(R.id.ll_box90) LinearLayout box90;
    @BindView(R.id.ll_box91) LinearLayout box91;
    @BindView(R.id.ll_box92) LinearLayout box92;
    @BindView(R.id.ll_box93) LinearLayout box93;
    @BindView(R.id.ll_box94) LinearLayout box94;
    @BindView(R.id.ll_box95) LinearLayout box95;
    @BindView(R.id.ll_box96) LinearLayout box96;
    @BindView(R.id.ll_box97) LinearLayout box97;
    @BindView(R.id.ll_box98) LinearLayout box98;
    @BindView(R.id.ll_box99) LinearLayout box99;
    @BindView(R.id.ll_box100) LinearLayout box100;
    @BindView(R.id.ll_box101) LinearLayout box101;
    @BindView(R.id.ll_box102) LinearLayout box102;
    @BindView(R.id.ll_box103) LinearLayout box103;
    @BindView(R.id.ll_box104)  LinearLayout box104;
    @BindView(R.id.ll_box105)  LinearLayout box105;
    @BindView(R.id.ll_box106)  LinearLayout box106;
    @BindView(R.id.ll_box107)  LinearLayout box107;
    @BindView(R.id.ll_box108)  LinearLayout box108;
    @BindView(R.id.ll_box109)  LinearLayout box109;
    @BindView(R.id.ll_box110)  LinearLayout box110;
    @BindView(R.id.ll_box111)  LinearLayout box111;
    @BindView(R.id.ll_box112)  LinearLayout box112;
    @BindView(R.id.ll_box113)  LinearLayout box113;
    @BindView(R.id.ll_box114)  LinearLayout box114;
    @BindView(R.id.ll_box115)  LinearLayout box115;
    @BindView(R.id.ll_box116)  LinearLayout box116;
    @BindView(R.id.ll_box117)  LinearLayout box117;
    @BindView(R.id.ll_box118) LinearLayout box118;
    @BindView(R.id.ll_boxlan) LinearLayout boxlan;
    @BindView(R.id.ll_boxact) LinearLayout boxact;


    // Utility images, buttons and custom displays
    ImageView chemist;
    TextView color;
    TextView element_name;
    TextView value;
    String selection;
    String dropdown_colortype;
    ArrayList<Element> list;


    //Box where the random element is displayed from the class Elements

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //restore any saved state
        super.onCreate( savedInstanceState );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN );
        //set content view
        setRequestedOrientation( SCREEN_ORIENTATION_LANDSCAPE );
        // set content view lala
        setContentView( R.layout.activity_table_info );
        ButterKnife.bind( this );

        //Initialise Elements


        //Initialise UI elements

        chemist = (ImageView) findViewById( R.id.anim );
        chemist.setBackgroundResource( R.drawable.livre_list );
        AnimationDrawable livAnimation = (AnimationDrawable) chemist.getBackground();
        livAnimation.start();
        element_name = (TextView) findViewById( R.id.element_name );
        value = (TextView) findViewById( R.id.display_info_text );
        box1.setBackgroundResource( R.drawable.rightbox );
        //String dropdown_colortype = "elconf";


        list = new ArrayList<Element>();

      list.add(new Element (R.string.helium,R.string.helium_hint, R.string.helium_info, R.string.helium_name, R.string.ngases, box2, null, R.string.helium_el_config, 4.003f, 24.587f, 0, 0.95f, 4.22f, 120 ));
        list.add(new Element (R.string.lithium,R.string.lithium_hint, R.string.lithium_info, R.string.lithium_name, R.string.alkaline, box3, null, R.string.lithium_el_config, 6.94f, 5.392f, 0.98f, 453.69f, 1560.0f, 145 ));
        list.add(new Element (R.string.berillium, R.string.berillium_hint, R.string.berillium_info, R.string.berillium_name, R.string.alk_earth, box4, null, R.string.berillium_el_config, 9.012f, 9.323f, 1.57f, 1560.0f, 2742.0f, 105 ));
        list.add(new Element (R.string.boron,R.string.boron_hint, R.string.boron_info, R.string.boron_name, R.string.metalloids, box5, null, R.string.boron_el_config, 10.81f, 8.298f, 2.04f, 2349.0f, 4200.0f, 85 ));
        list.add(new Element (R.string.carbon,R.string.carbon_hint, R.string.carbon_info, R.string.carbon_name, R.string.nmetals, box6, null, R.string.carbon_el_config, 12.011f, 11.26f, 2.55f, 3800.0f, 4300.0f, 70) );
        list.add(new Element (R.string.nitrogen,R.string.nitrogen_hint, R.string.nitrogen_info, R.string.nitrogen_name, R.string.nmetals, box7, null, R.string.nitrogen_el_config, 14.007f, 14.534f, 3.04f, 63.15f, 77.36f, 65 ));
        list.add(new Element (R.string.oxygen,R.string.oxygen_hint, R.string.oxygen_info, R.string.oxygen_name, R.string.nmetals, box8, null, R.string.oxygen_el_config, 15.999f, 13.618f, 3.44f, 54.36f, 90.20f, 60 ));
        list.add(new Element (R.string.fluorine,R.string.fluorine_hint, R.string.fluorine_info, R.string.fluorine_name, R.string.halogens, box9, null, R.string.fluorine_el_config, 18.996f, 17.423f, 3.98f, 53.53f, 85.03f, 50 ));
        list.add(new Element (R.string.neon,R.string.neon_hint, R.string.neon_info, R.string.neon_name, R.string.ngases, box10, null, R.string.neon_el_config, 20.18f, 21.565f, 0, 24.56f, 27.07f, 160 ));

        list.add(new Element (R.string.sodium, R.string.sodium_hint, R.string.sodium_info, R.string.sodium_name, R.string.alkaline, box11, null, R.string.sodium_el_config, 22.99f, 5.139f, 0.93f, 370.87f, 1156.0f, 180  ));
        list.add(new Element (R.string.magnesium,R.string.magnesium_hint, R.string.magnesium_info, R.string.magnesium_name, R.string.alk_earth, box12, null, R.string.magnesium_el_config, 24.305f, 7.646f, 1.31f, 923.0f, 1363.0f, 150 ));
        list.add(new Element (R.string.aluminium,R.string.aluminium_hint, R.string.aluminium_info, R.string.aluminium_name, R.string.posttm, box13, null, R.string.aluminium_el_config, 26.982f, 5.986f, 1.61f, 933.47f, 2792.0f, 125 ));
        list.add(new Element (R.string.silicon,R.string.silicon_hint, R.string.silicon_info, R.string.silicon_name, R.string.metalloids, box14, null, R.string.silicon_el_config, 28.085f, 8.152f, 1.90f, 1687.0f, 3538.0f, 110 ));
        list.add(new Element (R.string.phosphorous,R.string.phosphorous_hint, R.string.phosphorous_info, R.string.phosphorous_name, R.string.nmetals, box15, null, R.string.phosphorous_el_config, 30.974f, 10.487f, 2.19f, 317.30f, 550.0f, 100 ));
        list.add(new Element (R.string.sulfur,R.string.sulfur_hint, R.string.sulfur_info, R.string.sulfur_name, R.string.nmetals, box16, null, R.string.sodium_el_config, 32.06f, 10.36f, 2.58f, 388.36f, 717.87f, 100) );
        list.add(new Element (R.string.chlorine,R.string.chlorine_hint, R.string.chlorine_info, R.string.chlorine_name, R.string.halogens, box17, null, R.string.chlorine_el_config, 35.45f, 12.968f, 3.16f, 171.6f, 239.11f, 100 ));
        list.add(new Element (R.string.argon,R.string.argon_hint, R.string.argon_info, R.string.argon_name, R.string.ngases, box18, null, R.string.argon_el_config, 39.948f, 15.76f, 0, 83.80f, 87.30f, 71 ));
        list.add(new Element (R.string.potassium, R.string.potassium_hint, R.string.potassium_info, R.string.potassium_name, R.string.alkaline, box19, null, R.string.potassium_el_config, 39.098f, 4.341f, 0.82f, 336.53f, 1032.0f, 220 ));
        list.add(new Element (R.string.calcium,R.string.calcium_hint, R.string.calcium_info, R.string.calcium_name, R.string.alk_earth, box20, null, R.string.calcium_el_config, 40.078f, 6.113f, 1.0f, 1115.0f, 1757.0f, 180 ));

        list.add(new Element (R.string.scandium,R.string.scandium_hint, R.string.scandium_info, R.string.scandium_name, R.string.tmetals, box21, null, R.string.scandium_el_config, 44.956f, 6.562f, 1.36f, 1814.0f, 3109.0f, 160 ));
        list.add(new Element (R.string.titanium,R.string.titanium_hint, R.string.titanium_info, R.string.titanium_name, R.string.tmetals, box22, null, R.string.titanium_el_config, 47.867f, 6.828f, 1.54f, 1941.0f, 3560.0f, 140 ));
        list.add(new Element (R.string.vanadium,R.string.vanadium_hint, R.string.vanadium_info, R.string.vanadium_name, R.string.tmetals, box23, null, R.string.vanadium_el_config, 50.942f, 6.746f, 1.63f, 2183.0f, 3680.0f, 135 ));
        list.add(new Element (R.string.chromium,R.string.chromium_hint, R.string.chromium_info, R.string.chromium_name, R.string.tmetals, box24, null, R.string.chromium_el_config, 51.996f, 6.767f, 1.66f, 2180.0f, 2944.0f, 140 ));
        list.add(new Element (R.string.manganese,R.string.manganese_hint, R.string.manganese_info, R.string.manganese_name, R.string.tmetals, box25, null, R.string.manganese_el_config, 54.938f, 7.434f, 1.55f, 1519.0f, 2334.0f, 140 ));
        list.add(new Element (R.string.iron,R.string.iron_hint, R.string.iron_info, R.string.iron_name, R.string.tmetals, box26, null, R.string.iron_el_config, 55.845f, 7.903f, 1.83f, 1811.0f, 3134.0f, 140 ));
        list.add(new Element (R.string.cobalt,R.string.cobalt_hint, R.string.cobalt_info, R.string.cobalt_name, R.string.tmetals, box27, null, R.string.cobalt_el_config, 58.993f, 7.881f, 1.88f, 1768.0f, 3200.0f, 135 ));
        list.add(new Element (R.string.nickel,R.string.nickel_hint, R.string.nickel_info, R.string.nickel_name, R.string.tmetals, box28, null, R.string.nickel_el_config, 58.693f, 7.64f, 1.91f, 1728.0f, 3186.0f, 135 ));
        list.add(new Element (R.string.copper,R.string.copper_hint, R.string.copper_info, R.string.copper_name, R.string.tmetals, box29, null, R.string.copper_el_config, 63.346f, 7.726f, 1.90f, 1357.77f, 2835.0f, 135 ));
        list.add(new Element (R.string.zinc,R.string.zinc_hint, R.string.zinc_info, R.string.zinc_name, R.string.tmetals, box30, null, R.string.zinc_el_config, 65.38f, 9.394f, 1.65f, 692.88f, 1180, 135 ));

        list.add(new Element (R.string.gallium,R.string.gallium_hint, R.string.gallium_info, R.string.gallium_name, R.string.posttm, box31, null, R.string.gallium_el_config, 69.723f, 5.999f, 1.81f, 302.91f, 2673.0f, 130 ));
        list.add(new Element (R.string.germanium,R.string.germanium_hint, R.string.germanium_info, R.string.germanium_name, R.string.metalloids, box32, null, R.string.germanium_el_config, 72.63f, 7.899f, 2.01f, 1211.40f, 3106.0f, 125 ));
        list.add(new Element (R.string.arsenic,R.string.arsenic_hint, R.string.arsenic_info, R.string.arsenic_name, R.string.metalloids, box33, null, R.string.arsenic_el_config, 74.922f, 9.789f, 2.18f, 1090.0f, 887.0f, 115 ));
        list.add( new Element (R.string.selenium,R.string.selenium_hint, R.string.selenium_info, R.string.selenium_name, R.string.nmetals, box34, null, R.string.selenium_el_config, 78.971f, 9.752f, 2.55f, 453.0f, 958.0f, 115 ));
        list.add( new Element (R.string.bromine,R.string.bromine_hint, R.string.bromine_info, R.string.bromine_name, R.string.halogens, box35, null, R.string.bromine_el_config, 79.904f, 11.814f, 2.96f, 265.8f, 332.0f, 115 ));
        list.add(new Element (R.string.krypton,R.string.krypton_hint, R.string.krypton_info, R.string.krypton_name, R.string.ngases, box36, null, R.string.krypton_el_config, 83.798f, 14.0f, 3.00f, 115.79f, 119.93f, 0 ));
        list.add(new Element (R.string.rubidium, R.string.rubidium_hint, R.string.rubidium_info,R.string.rubidium_name, R.string.alkaline, box37, null, R.string.rubidium_el_config, 85.468f, 4.177f, 0.82f, 312.46f, 961.0f, 235));
        list.add( new Element (R.string.strontium,R.string.stroncium_hint, R.string.stroncium_info, R.string.strontium_name, R.string.alk_earth, box38, null, R.string.stroncium_el_config, 87.62f, 5.695f, 0.95f, 1050.0f, 1655.0f, 200 ));
        list.add(new Element (R.string.yttrium,R.string.yttrium_hint, R.string.yttrium_info, R.string.ytterbium_name, R.string.tmetals, box39, null, R.string.yttrium_el_config, 88.906f, 6.217f, 1.22f, 1799.0f, 3609.0f, 180 ));
        list.add(new Element (R.string.zirconium,R.string.zirconium_hint, R.string.zirconium_info, R.string.zirconium_name, R.string.tmetals, box40, null, R.string.zirconium_el_config, 91.224f, 6.634f, 1.33f, 2128.0f, 4682.0f, 155 ));

        list.add(new Element (R.string.niobium,R.string.niobium_hint, R.string.niobium_info, R.string.niobium_name, R.string.tmetals, box41, null, R.string.niobium_el_config, 92.906f, 6.759f, 1.6f, 2750.0f, 5017.0f, 145 ));
        list.add(new Element (R.string.molybdenum,R.string.molybdenum_hint, R.string.molybdenum_info, R.string.molybdenum_name, R.string.tmetals, box42, null, R.string.molybdenum_el_config, 95.95f, 7.092f, 2.16f, 2896.0f, 4912.0f, 145 ));
        list.add(new Element (R.string.technetium,R.string.technetium_hint, R.string.technetium_info, R.string.technetium_name, R.string.tmetals, box43,null, R.string.technetium_el_config, 98.0f, 7.119f, 1.9f, 2430.0f, 4538.0f, 135 ));
        list.add(new Element (R.string.ruthenium,R.string.ruthenium_hint, R.string.ruthenium_info, R.string.ruthenium_name, R.string.tmetals, box44, null, R.string.ruthenium_el_config, 101.07f, 7.361f, 2.2f, 2607.0f, 4423.0f, 130 ));
        list.add(new Element (R.string.rhodium,R.string.rhodium_hint, R.string.rhodium_info, R.string.rhodium_name, R.string.tmetals, box45, null, R.string.rhodium_el_config, 102.906f, 7.459f, 2.28f, 2237.0f, 3968.0f, 135) );
        list.add(new Element (R.string.palladium,R.string.palladium_hint, R.string.palladium_info, R.string.palladium_name, R.string.tmetals, box46, null, R.string.palladium_el_config, 106.42f, 8.337f, 2.20f, 1828.05f, 3236.0f, 140 ));
        list.add(new Element (R.string.silver,R.string.silver_hint, R.string.silver_info, R.string.silver_name, R.string.tmetals, box47, null, R.string.silver_el_config, 107.868f, 7.576f, 1.93f, 1234.93f, 2435.0f, 160 ));
        list.add(new Element (R.string.cadmium,R.string.cadmium_hint, R.string.cadmium_info, R.string.cadmium_name, R.string.tmetals, box48, null, R.string.cadmium_el_config, 112.414f, 8.994f, 1.69f, 594.22f, 1040.0f, 155 ));
        list.add(new Element (R.string.indium,R.string.indium_hint, R.string.indium_info, R.string.indium_name, R.string.posttm, box49, null, R.string.indium_el_config, 114.818f, 5.786f, 1.78f, 429.75f, 2345.0f, 155 ));
        list.add(new Element (R.string.tin,R.string.tin_hint, R.string.tin_info, R.string.tin_name, R.string.posttm, box50, null, R.string.tin_el_config, 118.71f, 7.344f, 1.96f, 505.08f, 2875.0f, 145 ));

        list.add(new Element (R.string.antimony,R.string.antimony_hint, R.string.antimony_info, R.string.antimony_name, R.string.metalloids, box51, null, R.string.antimony_el_config, 121.76f, 8.608f, 2.05f, 903.78f, 1860.0f, 145 ));
        list.add(new Element (R.string.tellurium,R.string.tellurium_hint, R.string.tellurium_info, R.string.tellurium_name, R.string.metalloids, box52, null, R.string.tellurium_el_config, 127.6f, 9.01f, 2.1f, 722.66f, 1261.0f, 140 ));
        list.add(new Element (R.string.iodine,R.string.iodine_hint, R.string.iodine_info, R.string.iodine_name, R.string.halogens, box53, null, R.string.iodine_el_config, 126.904f, 10.451f, 2.66f, 386.85f, 457.4f, 140 ));
        list.add(new Element (R.string.xenon, R.string.xenon_hint, R.string.xenon_info, R.string.xenon_name, R.string.ngases, box54, null, R.string.xenon_el_config, 131.293f, 12.129f, 2.60f, 161.4f, 165.03f, 0 ));
        list.add(new Element (R.string.caesium,R.string.caesium_hint, R.string.caesium_info, R.string.caesium_name, R.string.alkaline, box55, null, R.string.caesium_el_config, 132.905f, 3.894f, 0.79f, 301.59f, 944.0f, 260 ));
        list.add(new Element (R.string.barium,R.string.barium_hint, R.string.barium_info, R.string.barium_name, R.string.alk_earth, box56, null, R.string.barium_el_config, 137.327f, 5.212f, 0.89f, 1000.0f, 2170.0f, 215) );
        list.add(new Element (R.string.lanthanum,R.string.lanthanum_hint, R.string.lanthanum_info, R.string.lanthanum_name, R.string.lanthanides, box57, null, R.string.lanthanum_el_config, 138.905f, 5.577f, 1.10f, 1193.0f, 3737.0f, 195 ));
        list.add(new Element (R.string.cerium,R.string.cerium_hint, R.string.cerium_info, R.string.cerium_name, R.string.lanthanides, box58, null, R.string.cerium_el_config, 140.116f, 5.539f, 1.12f, 1068.0f, 3716.0f, 185) );
        list.add(new Element (R.string.praseodymium,R.string.praseodymium_hint, R.string.praseodymium_info, R.string.praseodymium_name, R.string.lanthanides, box59, null, R.string.praseodymium_el_config, 140.908f, 5.47f, 1.13f, 1208.0f, 3793.0f, 185 ));
        list.add(new Element (R.string.neodymium,R.string.neodymium_hint, R.string.neodymium_info, R.string.neodymium_name, R.string.lanthanides, box60, null, R.string.neodymium_el_config, 144.242f, 5.525f, 1.14f, 1297.0f, 3347.0f, 185 ));

        list.add(new Element (R.string.promethium, R.string.promethium_hint, R.string.promethium_info, R.string.promethium_name, R.string.lanthanides, box61, null, R.string.promethium_el_config, 145.0f, 5.577f, 1.13f, 1315.0f, 3273.0f, 185  ));
        list.add(new Element (R.string.samarium,R.string.samarium_hint, R.string.samarium_info, R.string.samarium_name, R.string.lanthanides, box62, null, R.string.samarium_el_config, 150.36f, 5.644f, 1.17f, 1345.0f, 2067.0f, 185 ));
        list.add(new Element (R.string.europium,R.string.europium_hint, R.string.europium_info, R.string.europium_name, R.string.lanthanides, box63, null, R.string.europium_el_config, 151.964f, 5.67f, 1.20f, 1099.0f, 1802.0f, 185) );
        list.add(new Element (R.string.gadolinium,R.string.gadolinium_hint, R.string.gadolinium_info, R.string.gadolinium_name, R.string.lanthanides, box64, null, R.string.gadolinium_el_config, 157.25f, 6.15f, 1.20f, 1585.0f, 3546.0f, 180 ));
        list.add(new Element (R.string.terbium,R.string.terbium_hint, R.string.terbium_info, R.string.terbium_name, R.string.lanthanides, box65, null, R.string.terbium_el_config, 158.925f, 5.864f, 1.20f, 1629.0f, 3503.0f, 175 ));
        list.add(new Element (R.string.dysprosium,R.string.dysprosium_hint, R.string.dysprosium_info, R.string.dysprosium_name, R.string.lanthanides, box66, null, R.string.dysprosium_el_config, 162.5f, 5.939f, 1.22f, 1680.0f, 2840.0f, 175 ));
        list.add(new Element (R.string.holmium,R.string.holmium_hint, R.string.holmium_info, R.string.holmium_name, R.string.lanthanides, box67, null, R.string.holmium_el_config, 164.93f, 6.022f, 1.23f, 1734.0f, 2993.0f, 175 ));
        list.add(new Element (R.string.erbium,R.string.erbium_hint, R.string.erbium_info, R.string.erbium_name, R.string.lanthanides, box68, null, R.string.erbium_el_config, 167.259f, 6.108f, 1.24f, 1802.0f, 3141.0f, 175 ));
        list.add(new Element (R.string.thulium, R.string.thulium_hint, R.string.thulium_info, R.string.thulium_name, R.string.lanthanides, box69, null, R.string.thulium_el_config, 168.934f, 6.184f, 1.25f, 1818.0f, 2223.0f, 175 ));
        list.add(new Element (R.string.ytterbium,R.string.ytterbium_hint, R.string.ytterbium_info, R.string.ytterbium_name, R.string.lanthanides, box70, null, R.string.ytterbium_el_config, 173.045f, 6.254f, 1.10f, 1097.0f, 1469.0f, 175 ));

        list.add(new Element (R.string.lutetium,R.string.lutetium_hint, R.string.lutetium_info, R.string.lutetium_name, R.string.lanthanides, box71, null, R.string.lutetium_el_config, 174.968f, 5.426f, 1.27f, 1925.0f, 3675.0f, 175 ));
        list.add(new Element (R.string.hafnium,R.string.hafnium_hint, R.string.hafnium_info, R.string.hafnium_name, R.string.tmetals, box72, null, R.string.hafnium_el_config, 178.49f, 6.825f, 1.30f, 2506.0f, 4876.0f, 155 ));
        list.add(new Element (R.string.tantalum,R.string.tantalum_hint, R.string.tantalum_info, R.string.tantalum_name, R.string.tmetals, box73, null, R.string.tantalum_el_config, 180.948f, 7.55f, 1.50f, 3290.0f, 5731.0f, 145 ));
        list.add(new Element (R.string.tungsten,R.string.tungsten_hint, R.string.tungsten_info, R.string.tungsten_name, R.string.tmetals, box74, null, R.string.tungsten_el_config, 183.84f, 7.864f, 2.36f, 3695.0f, 5828.0f, 135 ));
        list.add(new Element (R.string.rhenium,R.string.rhenium_hint, R.string.rhenium_info, R.string.rhenium_name, R.string.tmetals, box75, null, R.string.rhenium_el_config, 186.207f, 7.834f, 1.90f, 3459.0f, 5869.0f, 135 ));
        list.add(new Element (R.string.osmium,R.string.osmium_hint, R.string.osmium_info, R.string.osmium_name, R.string.tmetals, box76, null, R.string.osmium_el_config, 190.23f, 8.438f, 2.20f, 3306.0f, 5285.0f, 130 ));
        list.add(new Element (R.string.iridium,R.string.iridium_hint, R.string.iridium_info, R.string.iridium_name, R.string.tmetals, box77, null, R.string.iridium_el_config, 192.217f, 8.967f, 2.20f, 2719.0f, 4701.0f, 135 ));
        list.add(new Element (R.string.platinum,R.string.platinum_hint, R.string.platinum_info, R.string.platinum_name, R.string.tmetals, box78, null, R.string.platinum_el_config, 195.084f, 8.959f, 2.28f, 2041.4f, 4098.0f, 135 ));
        list.add(new Element (R.string.gold,R.string.gold_hint, R.string.gold_info, R.string.gold_name, R.string.tmetals, box79, null, R.string.gold_el_config, 196.967f, 9.226f, 2.54f, 1337.33f, 3129.0f, 135 ));
        list.add(new Element (R.string.mercury,R.string.mercury_hint, R.string.mercury_info, R.string.mercury_name, R.string.tmetals, box80, null, R.string.mercury_el_config, 200.592f, 10.438f, 2.00f, 234.43f, 629.88f, 150 ));

        list.add(new Element (R.string.thallium,R.string.thallium_hint, R.string.thallium_info, R.string.thallium_name, R.string.posttm, box81,null, R.string.thallium_el_config, 204.38f, 6.108f, 1.62f, 577.0f, 1746.0f, 190 ));
        list.add(new Element (R.string.lead,R.string.lead_hint, R.string.lead_info, R.string.lead_name, R.string.posttm, box82, null, R.string.lead_el_config, 207.2f, 7.417f, 1.87f, 600.61f, 2022.0f, 180) );
        list.add(new Element (R.string.bismuth,R.string.bismuth_hint, R.string.bismuth_info, R.string.bismuth_name, R.string.posttm, box83, null, R.string.bismuth_el_config, 208.98f, 7.286f, 2.02f, 544.7f, 1837.0f, 160 ));
        list.add(new Element (R.string.polonium,R.string.polonium_hint, R.string.polonium_info, R.string.polonium_name, R.string.metalloids, box84, null, R.string.polonium_el_config, 209.0f, 8.414f, 2.00f, 527.0f, 1235.0f, 190 ));
        list.add(new Element (R.string.astatine,R.string.astatine_hint, R.string.astatine_info, R.string.astatine_name, R.string.halogens, box85,null, R.string.astatine_el_config, 210.0f, 9.318f, 2.20f, 575.0f, 610.0f, 0 ));
        list.add(new Element (R.string.radon,R.string.radon_hint, R.string.radon_info, R.string.radon_name, R.string.ngases, box86, null, R.string.radon_el_config, 222.0f, 10.749f, 2.20f, 202.0f, 211.3f, 0 ));
        list.add(new Element (R.string.francium, R.string.francium_hint, R.string.francium_info,R.string.francium_name, R.string.alkaline, box87, null, R.string.francium_el_config, 223.0f, 4.073f, 0.70f, 300.0f, 950.0f, 0));
        list.add(new Element (R.string.radium,R.string.radium_hint, R.string.radium_info, R.string.radium_name, R.string.alk_earth, box88, null, R.string.radium_el_config, 226.0f, 5.278f, 0.90f, 973.0f, 2010.0f, 215 ));
        list.add(new Element (R.string.actinium,R.string.actinium_hint, R.string.actinium_info, R.string.actinium_name, R.string.actinides, box89, null, R.string.actinium_el_config, 277.0f, 5.38f, 1.10f, 1323.0f, 3471.0f, 195 ));
        list.add(new Element (R.string.thorium,R.string.thorium_hint, R.string.thorium_info, R.string.thorium_name, R.string.actinides, box90, null, R.string.thorium_el_config, 232.038f, 6.307f, 1.30f, 2115.0f, 5061.0f, 180 ));

        list.add(new Element (R.string.protactinium,R.string.protactinium_hint, R.string.protactinium_info, R.string.protactinium_name, R.string.actinides, box91, null, R.string.protactinium_el_config, 231.036f, 5.89f, 1.50f, 1841.0f, 4300.0f, 180 ));
        list.add(new Element (R.string.uranium,R.string.uranium_hint, R.string.uranium_info, R.string.uranium_name, R.string.actinides, box92, null, R.string.uranium_el_config, 238.029f, 6.194f, 1.38f, 1405.3f, 4404.0f, 175 ));
        list.add(new Element (R.string.neptunium,R.string.neptunium_hint, R.string.neptunium_info, R.string.neptunium_name, R.string.actinides, box93, null, R.string.neptunium_el_config, 237.0f, 6.266f, 1.36f, 917.0f, 4273.0f, 175 ));
        list.add(new Element (R.string.plutonium,R.string.plutonium_hint, R.string.plutonium_info, R.string.plutonium_name, R.string.actinides, box94, null, R.string.plutonium_el_config, 244.0f, 6.026f, 1.28f, 912.5f, 3501.0f, 175 ));
        list.add(new Element (R.string.americium,R.string.americium_hint, R.string.americium_info, R.string.americium_name, R.string.actinides, box95, null, R.string.americium_el_config, 243.0f, 5.974f, 1.13f, 1449.0f, 2880.0f, 175 ));
        list.add(new Element (R.string.curium,R.string.curium_hint, R.string.curium_info, R.string.curium_name, R.string.actinides, box96, null, R.string.curium_el_config, 247.0f, 5.991f, 1.28f, 1613.0f, 3383.0f, 0 ));
        list.add(new Element (R.string.berkelium,R.string.berkelium_hint, R.string.berkelium_info, R.string.berkelium_name, R.string.actinides, box97, null, R.string.berkelium_el_config, 247.0f, 6.198f, 1.30f, 1259.0f, 2900.0f, 0 ));
        list.add(new Element (R.string.californium,R.string.californium_hint, R.string.californium_info, R.string.californium_name, R.string.actinides, box98, null, R.string.californium_el_config, 251.0f, 6.282f, 1.30f, 1173.0f, 1743.0f, 0 ));
        list.add(new Element (R.string.einsteinium,R.string.einsteinium_hint, R.string.einsteinium_info, R.string.einsteinium_name, R.string.actinides, box99, null, R.string.einsteinium_el_config, 252.0f, 6.368f, 1.30f, 1133.0f, 1269.0f, 0 ));
        list.add(new Element (R.string.fermium,R.string.fermium_hint, R.string.fermium_info, R.string.fermium_name, R.string.actinides, box100, null, R.string.fermium_el_config, 257.0f, 6.50f, 1.30f, 1125.0f, 0, 0 ));

        list.add(new Element (R.string.mendelevium,R.string.mendelevium_hint, R.string.mendelevium_info, R.string.mendelevium_name, R.string.actinides, box101, null, R.string.mendelevium_el_config, 258.0f, 6.58f, 1.30f, 1100.0f, 0, 0 ));
        list.add(new Element (R.string.nobelium,R.string.nobelium_hint, R.string.nobelium_info, R.string.nobelium_name, R.string.actinides, box102, null, R.string.nobelium_el_config, 259.0f, 6.66f, 1.30f, 1100.0f, 0, 0 ));
        list.add(new Element (R.string.lawrencium,R.string.lawrencium_hint, R.string.lawrencium_info, R.string.lawrencium_name, R.string.actinides, box103, null, R.string.lawrencium_el_config, 266.0f, 4.96f, 1.30f, 1900.0f, 0, 0 ));
        list.add(new Element (R.string.rutherfordium, R.string.rutherfordium_hint, R.string.rutherfordium_info, R.string.rutherfordium_name, R.string.tmetals, box104, null, R.string.rutherfordium_el_config, 267.0f, 6.02f, 0, 2400.0f, 5800.0f, 0 ));
        list.add(new Element (R.string.dubnium,R.string.dubnium_hint, R.string.dubnium_info, R.string.dubnium_name, R.string.tmetals, box105, null, R.string.dubnium_el_config, 268.0f, 6.8f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.seaborgium,R.string.seaborgium_hint, R.string.seaborgium_info, R.string.seaborgium_name, R.string.tmetals, box106, null, R.string.seaborgium_el_config, 271.0f, 7.8f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.bohrium,R.string.bohrium_hint, R.string.bohrium_info, R.string.bohrium_name, R.string.tmetals, box107, null, R.string.bohrium_el_config, 270.0f, 7.7f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.hassium,R.string.hassium_hint, R.string.hassium_info, R.string.hassium_name, R.string.tmetals, box108, null, R.string.hassium_el_config, 269.0f, 7.6f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.meitnerium,R.string.meitnerium_hint, R.string.meitnerium_info, R.string.meitnerium_name, R.string.unknown_type, box109, null, R.string.meitnerium_el_config, 278.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.darmstadtium,R.string.darmstadtium_hint, R.string.darmstadtium_info, R.string.darmstadtium_name, R.string.unknown_type, box110, null, R.string.darmstadtium_el_config, 281.0f, 0.0f, 0, 0, 0, 0 ));

        list.add(new Element (R.string.roentgenium, R.string.roentgenium_hint, R.string.roentgenium_info, R.string.roentgenium_name, R.string.unknown_type, box111, null, R.string.roentgenium_el_config, 282.0f, 0.0f, 0, 0, 0, 0  ));
        list.add(new Element (R.string.copernicium,R.string.copernicium_hint, R.string.copernicium_info, R.string.copernicium_name, R.string.unknown_type, box112, null, R.string.copernicium_el_config, 285.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.nihonium,R.string.nihonium_hint, R.string.nihonium_info, R.string.nihonium_name, R.string.unknown_type, box113, null, R.string.nihonium_el_config, 286.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.flerovium,R.string.flerovium_hint, R.string.flerovium_info, R.string.flerovium_name, R.string.unknown_type, box114, null, R.string.flerovium_el_config, 289.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.moscovium,R.string.moscovium_hint, R.string.moscovium_info, R.string.moscovium_name, R.string.unknown_type, box115,null, R.string.moscovium_el_config, 289.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.livermorium,R.string.livermorium_hint, R.string.livermorium_info, R.string.livermorium_name, R.string.unknown_type, box116, null, R.string.livermorium_el_config, 293.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.tennessine,R.string.tennessine_hint, R.string.tennessine_info, R.string.tennessine_name, R.string.unknown_type, box117, null, R.string.tennessine_el_config, 294.0f, 0.0f, 0, 0, 0, 0 ));
        list.add(new Element (R.string.oganesson,R.string.oganesson_hint, R.string.oganesson_info, R.string.oganesson_name, R.string.unknown_type, box118, null, R.string.oganesson_el_config, 294.0f, 0.0f, 0, 0, 0, 0) );
        list.add(new Element (R.string.hydrogen,R.string.hydrogen_hint, R.string.hydrogen_info, R.string.hydrogen_name, R.string.type_hydrogen, box1, null, R.string.hydrogen_el_config , 1.008f, 13.598f, 2.20f, 13.99f, 20.271f, 25 ));

        // Spinner element
        final Spinner spinner = (Spinner) findViewById( R.id.spinner );

        // Spinner click listener
        spinner.setOnItemSelectedListener( this );

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add( "Electron Config." );
        categories.add( "Atomic Mass" );
        categories.add( "1st Ionization E (eV)" );
        categories.add( "Electronegativity" );
        categories.add( "Melting Point (K)" );
        categories.add( "Boiling Point (K)" );
        categories.add( "Atomic Radius (pm)" );


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>( this, android.R.layout.simple_spinner_item, categories );

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        // attaching data adapter to spinner
        spinner.setAdapter( dataAdapter );

        //Set click listeners
        //defineprop(list, spinner );

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        final String item = adapterView.getItemAtPosition( position ).toString();
        for (int i =0; i < list.size(); i++ ) {
            final Element el = list.get( i );
            if (item.equalsIgnoreCase( "Electron Config." )) {
                dropdown_colortype = "elconf";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                value.setText(el.getElConfig());
                if (selection == getResources().getString( el.getSymbol() )) {
                    value.setText(el.getElConfig());

                }
            }

            else if (item.equalsIgnoreCase( "Atomic Mass" )) {
                dropdown_colortype = "amass";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String am= Float.toString( el.getAtomicMass() );
                if(am.equalsIgnoreCase( "0.0" )|| am.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( am );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(am.equalsIgnoreCase( "0.0" )|| am.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( am );}
                }
            }


            else if (item.equalsIgnoreCase( "1st Ionization E (eV)" )) {
                dropdown_colortype = "ionize";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String ion= Float.toString( el.getIonization_energy() );
                if(ion.equalsIgnoreCase( "0.0" )|| ion.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( ion );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(ion.equalsIgnoreCase( "0.0" )|| ion.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( ion );}
                }
            }

            else if (item.equalsIgnoreCase( "Electronegativity" )) {
                dropdown_colortype = "electroneg";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String en= Float.toString( el.getElectronegativity() );
                if(en.equalsIgnoreCase( "0.0" )|| en.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( en );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(en.equalsIgnoreCase( "0.0" )|| en.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( en );}
                }
            }

            else if (item.equalsIgnoreCase( "Melting Point (K)" )) {
                dropdown_colortype = "meltp";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String mp= Float.toString( el.getMelting_point() );
                if(mp.equalsIgnoreCase( "0.0" )|| mp.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( mp );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(mp.equalsIgnoreCase( "0.0" )|| mp.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( mp );}
                }
            }

            else if (item.equalsIgnoreCase( "Boiling Point (K)" )) {
                dropdown_colortype = "boilp";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String bp= Float.toString( el.getBoiling_point() );
                if(bp.equalsIgnoreCase( "0.0" )|| bp.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( bp );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(bp.equalsIgnoreCase( "0.0" )|| bp.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( bp );}
                }
            }

            else if (item.equalsIgnoreCase( "Atomic Radius (pm)" )) {
                dropdown_colortype = "aradius";
                reset_colors();
                element_name.setText( getResources().getString( el.getName() ) );
                String ar= Integer.toString( el.getAtomic_radius() );
                if(ar.equalsIgnoreCase( "0.0" )|| ar.equalsIgnoreCase( "0" )){
                    value.setText( "-" );
                }
                else {value.setText( ar );}
                if (selection == getResources().getString( el.getSymbol() )) {
                    if(ar.equalsIgnoreCase( "0.0" )|| ar.equalsIgnoreCase( "0" )){
                        value.setText( "-" );
                    }
                    else {value.setText( ar );}
                }
            }



            View v = el.getPosition();
            v.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    selection = getResources().getString(el.getSymbol());
                    String s = item;
                    reset_colors( );
                    v.setBackgroundResource( R.drawable.rightbox );
                    if (s.equalsIgnoreCase( "Electron Config." )) {
                        dropdown_colortype = "elconf";
                        value.setText(el.getElConfig());
                        element_name.setText( getResources().getString(el.getName() ));
                    }
                    else if (s.equalsIgnoreCase( "Atomic mass" )) {
                        dropdown_colortype = "amass";
                        value.setText( Float.toString(el.getAtomicMass() ));
                        element_name.setText( getResources().getString(el.getName() ));
                    }

                    else if (s.equalsIgnoreCase( "1st Ionization E (eV)" )) {
                        dropdown_colortype = "ionize";
                        String ion =  Float.toString(el.getIonization_energy() );
                        if(ion.equalsIgnoreCase( "0.0" )|| ion.equalsIgnoreCase( "0" )){
                            value.setText( "-" );
                        }
                        else {value.setText( ion );}
                        element_name.setText( getResources().getString(el.getName() ));
                    }

                    else if (s.equalsIgnoreCase( "Electronegativity" )) {
                        dropdown_colortype = "electroneg";
                        String en= Float.toString( el.getElectronegativity() );
                        if(en.equalsIgnoreCase( "0.0" )|| en.equalsIgnoreCase( "0" )){
                            value.setText( "-" );
                        }
                        else {value.setText( en );}
                        element_name.setText( getResources().getString(el.getName() ));
                    }

                    else if (s.equalsIgnoreCase( "Melting Point (K)" )) {
                        dropdown_colortype = "meltp";
                        String mp= Float.toString( el.getMelting_point() );
                        if(mp.equalsIgnoreCase( "0.0" )|| mp.equalsIgnoreCase( "0" )){
                            value.setText( "-" );
                        }
                        else {value.setText( mp );}
                        element_name.setText( getResources().getString(el.getName() ));
                    }

                    else if (s.equalsIgnoreCase( "Boiling Point (K)" )) {
                        dropdown_colortype = "boilp";
                        String bp= Float.toString( el.getBoiling_point() );
                        if(bp.equalsIgnoreCase( "0.0" )|| bp.equalsIgnoreCase( "0" )){
                            value.setText( "-" );
                        }
                        else {value.setText( bp );}
                        element_name.setText( getResources().getString(el.getName() ));
                    }

                    else if (s.equalsIgnoreCase( "Atomic Radius (pm)" )) {
                        dropdown_colortype = "aradius";
                        String ar= Integer.toString( el.getAtomic_radius() );
                        if(ar.equalsIgnoreCase( "0.0" )|| ar.equalsIgnoreCase( "0" )){
                            value.setText( "-" );
                        }
                        else {value.setText( ar );}
                        element_name.setText( getResources().getString(el.getName() ));
                    }


                }} );
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }



    public void reset_colors() {

        for (int i =0; i < list.size(); i++ ) {
            Element e = list.get( i );

            if (dropdown_colortype == "amass") {
                float k = e.getAtomicMass() / 294.0f;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }

            else if (dropdown_colortype == "ionize") {
                float k = e.getIonization_energy() / 21.565f;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }

            else if (dropdown_colortype == "electroneg") {
                float k = e.getElectronegativity() / 3.98f;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }

            else if (dropdown_colortype == "meltp") {
                float k = e.getMelting_point() / 3695.0f;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }

            else if (dropdown_colortype == "boilp") {
                float k = e.getBoiling_point() / 5869.0f;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }


            else if (dropdown_colortype == "aradius") {
                float k = e.getAtomic_radius() / (float)260;
                int resultColor = ColorUtils.blendARGB( getResources().getColor( R.color.white ), getResources().getColor( R.color.postTM ), k );
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius( 15 );
                shape.setStroke( 3, getResources().getColor( noir ) );
                shape.setColor( resultColor );
                e.getPosition().setBackground( shape );
                boxlan.setBackgroundResource( R.drawable.box2_unknown );
                boxact.setBackgroundResource( R.drawable.box2_unknown);
            }

            else {
                if (e.getType() == R.string.alkaline) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_alkaline );
                } else if (e.getType() == R.string.alk_earth) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_alkalineearth );
                } else if (e.getType() == R.string.type_hydrogen) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_hydrogen );
                } else if (e.getType() == R.string.ngases) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_noblegases );
                } else if (e.getType() == R.string.tmetals) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_tmetals );
                } else if (e.getType() == R.string.posttm) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_posttm );
                } else if (e.getType() == R.string.metalloids) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_metalloids );
                } else if (e.getType() == R.string.nmetals) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_nonmetals );
                } else if (e.getType() == R.string.halogens) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_halogens );
                } else if (e.getType() == R.string.unknown_type) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_unknown );
                } else if (e.getType() == R.string.lanthanides) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_lanthanides );
                } else if (e.getType() == R.string.actinides) {
                    e.getPosition().setBackgroundResource( R.drawable.box2_actinides );
                }
            }
        }
    }


}



