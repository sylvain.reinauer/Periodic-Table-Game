package com.periodictable.chemgames.periodictablememorizer;

import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Element {

    protected int symbole;
    protected int hint;
    protected int randomInfo;
    protected int nom;
    protected int type;
    protected LinearLayout position_tableau;
    protected TextView textview_element;
    protected int electron_configuration;
    protected float atomic_mass;
    protected float ionization_energy;
    protected float electronegativity;
    protected float melting_point;
    protected float boiling_point;
    protected int atomic_radius;




//Constructeur de la classe par défaut: méthode d’instance qui va se charger de créer un objet, ne prends aucun paramètre.Va juste réserver la memoire pour le futur objet. A le même nom que la classe.

    public Element () {
        symbole = R.string.hydrogen;
        hint = R.string.livermorium_hint;
        randomInfo = R.string.livermorium_info;
        nom= R.string.hydrogen_name;
        type= R.string.alkalimetals;
        position_tableau= null;
        textview_element = null;
        electron_configuration = R.string.hydrogen_el_config;
        atomic_mass = 1.0f;
        ionization_energy = 1.0f;
        electronegativity = 1.0f;
        melting_point = 1.0f;
        boiling_point = 1.0f;
        atomic_radius = 1;

    }


    //Constructeur avec des paramètres pour mettre des données dans l’objet.
    public Element (int el_symbol, int el_hint, int el_info, int el_name, int el_type, LinearLayout el_position, TextView el_text, int el_config, float el_amass, float el_ionization_energy, float el_electronegativity, float el_melting_point, float el_boiling_point, int el_atomic_radius ) {
        symbole = el_symbol;
        hint = el_hint;
        randomInfo = el_info;
        nom= el_name;
        type= el_type;
        position_tableau= el_position;
        textview_element = el_text;
        electron_configuration = el_config;
        atomic_mass = el_amass;
        ionization_energy = el_ionization_energy;
        electronegativity = el_electronegativity;
        melting_point = el_melting_point;
        boiling_point = el_boiling_point;
        atomic_radius = el_atomic_radius;
    }

// Accesseur: méthode qui permet d’accéder aux variables des objets en lecture

    public int getSymbol () {
        return symbole;
    }

    public int getHint() { return hint; }

    public int getRandomInfo() { return randomInfo; }

    public int getName () {
        return nom;
    }

    public int getType () {
        return type;
    }

    public LinearLayout getPosition () {
        return position_tableau;
    }

    public TextView getTextView () {
        return textview_element;
    }

    public int getElConfig () {
        return electron_configuration;
    }

    public float getAtomicMass () {
        return atomic_mass;
    }

    public float getIonization_energy () {
        return ionization_energy;
    }

    public float getElectronegativity () {
        return electronegativity;
    }

    public float getMelting_point () {
        return melting_point;
    }

    public float getBoiling_point () {
        return boiling_point;
    }

    public int getAtomic_radius () {
        return atomic_radius;
    }



// Mutateur: accède aux variables en écriture. Ne retournent aucune valeur, elles les mettent à jour.


    public void setSymbol ( int el_symbol) {
        symbole = el_symbol;
    }

    public void setName (int el_name) {
        nom= el_name;
    }

    public void setType (int el_type) {
        type= el_type;
    }


}
