package com.periodictable.chemgames.periodictablememorizer;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;


public class ScrollviewLevels extends AppCompatActivity {

    //UI

    @BindView(R.id.known) ConstraintLayout known;
    @BindView(R.id.unknown) ConstraintLayout unknown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN );

        setRequestedOrientation( SCREEN_ORIENTATION_LANDSCAPE );
        setContentView( R.layout.chose_play );
        ButterKnife.bind( this );
    }

    @OnClick({R.id.known, R.id.unknown})
    public void startAction (View v) {
        switch(v.getId()){
            case R.id.known:
                startActivity( new Intent( ScrollviewLevels.this, RecyclerViewLevelsKnownElement.class ) );
                break;
            case R.id.unknown:
                startActivity( new Intent( ScrollviewLevels.this, RecyclerViewLevelsUnknownElement.class  ) );
                break;
        }
    }


}
