package com.periodictable.chemgames.periodictablememorizer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import com.periodictable.chemgames.periodictablememorizer.R;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.view.DragEvent.ACTION_DROP;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class Instructions extends AppCompatActivity implements View.OnTouchListener, View.OnDragListener{
    //List of elements_images
    TextView textview_random;
    TextView textview_chlorine;
    TextView textview_rhenium;

    LinearLayout box0;
    LinearLayout box17;
    LinearLayout box75;



    // Utility images, buttons and custom displays
    TextView time;
    CountDownTimer t;
    ImageView chemist;
    TextView hint_button;
    TextView place_button;
    TextView hint_display_text;
    LinearLayout hint_display;
    TextView reset;
    TextView color;
    TextView error;
    ConstraintLayout end_tuto_display;
    Button playagain;
    Button gotogame;


    LinearLayout bcg_rectangle_random;
    LinearLayout bcg_rectangle_hint;
    LinearLayout bcg_rectangle_place;
    LinearLayout opaque;

    TextView rectangle_random;
    TextView rectangle_hint;
    TextView rectangle_place;
    TextView random_info_display_text;

    private Handler mHandler = new Handler();


//TODO: create following levels: linearly filled, every group (lanthanides, actinides, etc.)

    //Box where the random element is displayed from the class Elements

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //restore any saved state
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view
        setRequestedOrientation(SCREEN_ORIENTATION_LANDSCAPE);
        // set content view lala
        setContentView( R.layout.instructions);


        //Initialise Elements

        box0= (LinearLayout)findViewById( R.id.ll_box0 );
        box17= (LinearLayout)findViewById( R.id.ll_box17 );
        box75= (LinearLayout)findViewById( R.id.ll_box75 );


        textview_random=(TextView)findViewById( R.id.textview_random);
        textview_chlorine=(TextView)findViewById( R.id.textview_chlorine);
        textview_rhenium=(TextView)findViewById( R.id.textview_rhenium);

        //Initialise UI elements

        chemist= (ImageView)findViewById( R.id.anim );
        hint_button= (TextView)findViewById( R.id.hint_button );
        place_button= (TextView)findViewById( R.id.place_button );
        hint_display=(LinearLayout) findViewById( R.id.hint_display );
        hint_display_text= (TextView)findViewById( R.id.hint_display_text );
        reset=(TextView) findViewById( R.id.textview_reset );
        time = (TextView) findViewById( R.id.time );
        opaque = (LinearLayout)findViewById( R.id.opaque_layer );
        random_info_display_text=(TextView)findViewById( R.id.random_info_display_text );


        error= (TextView)findViewById( R.id.error );
        end_tuto_display = (ConstraintLayout)findViewById( R.id.end_tutorial_display );
        playagain = (Button)findViewById( R.id.playagain );
        gotogame = (Button)findViewById( R.id.playgame );

        //Arrows

        bcg_rectangle_random = (LinearLayout)findViewById( R.id.bcg_rectangle_random );
        bcg_rectangle_hint = (LinearLayout)findViewById( R.id.bcg_rectangle_hint );
        bcg_rectangle_place = (LinearLayout)findViewById( R.id.bcg_rectangle_place );

        rectangle_random = (TextView)findViewById( R.id.rectangle_random );
        rectangle_hint = (TextView)findViewById( R.id.rectangle_hint );
        rectangle_place = (TextView)findViewById( R.id.rectangle_place );


        //Random Element displayed in the upper box (to be placed)
        int w = new Random().nextInt(element.el.size());
        Integer xx= element.el.get( 1 );
        int k = xx.intValue();
        textview_random.setText(k);
        set_tag_to_elements (k);
        element.el.remove( xx );

        //Table (LinearLayouts) set listener

        box0.setOnTouchListener(this);

        ObjectAnimator animation = ObjectAnimator.ofFloat(box0, "translationY", 10f);
        animation.setDuration(1000);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(box0, "translationY", 0f);
        animation2.setDuration(1000);
        final AnimatorSet as = new AnimatorSet();
        as.playSequentially(animation, animation2);
        as.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                as.start();
            }
        });
        as.start();

        box17.setOnDragListener(this);
        box75.setOnDragListener(this);
        box75.setBackgroundResource( R.drawable.errbox );


    }

    //Link UI elements to actions


    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("WrongCall")
    @Override
    @SuppressWarnings("deprecation")
//TODO: make an if else statement to cancel drag and drop when timer is finished

    public boolean onTouch(View view, MotionEvent event) {

        MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(view);
        // View.DragShadowBuilder shadowBuilder= new  View.DragShadowBuilder  (view);
        ClipData data = ClipData.newPlainText("id", view.getResources().getResourceEntryName(view.getId()));
        view.startDrag(data, shadowBuilder, view, 0);
        chemist.clearAnimation();
        chemist.setBackgroundResource( R.drawable.jumo1 );
        hint_display.setVisibility( INVISIBLE );
        return true;

    }


    @SuppressLint({"WrongCall", "ResourceAsColor", "NewApi"})
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override

    public boolean onDrag(View v, DragEvent event) {


        switch (event.getAction()) {

            // signal for the start of a drag and drop operation
            case DragEvent.ACTION_DRAG_STARTED:
                break;

            // the drag point has entered the bounding box of the View
            case DragEvent.ACTION_DRAG_ENTERED:
                v.setBackgroundResource( R.drawable.box1_d );
                break;

            // the user has moved the drag shadow outside the bounding box of the View
            case DragEvent.ACTION_DRAG_EXITED:
                v.setBackgroundResource( R.drawable.box2 );

                if (v==box75){
                    v.setBackgroundResource( R.drawable.box_tuto );
                }

                if (v==box17 ){
                    v.setBackgroundResource( R.drawable.box2_halogens );
                }


                break;

            //drag shadow has been released,the drag point is within the bounding box of the View
            case ACTION_DROP :
                hint_display.setVisibility( INVISIBLE );
                v.setBackgroundResource( R.drawable.box2 );
                if (v==box75){
                    v.setBackgroundResource( R.drawable.box2_tmetals ); }
                if (v==box17 ){
                    v.setBackgroundResource( R.drawable.box2_halogens ); }


                if (v==box17 && textview_random.getTag()=="17" ){
                    textview_chlorine.setVisibility( VISIBLE);
                    Integer iInteger = new Integer(R.string.chlorine);
                    remove_el_from_db(iInteger);
                    box17.setOnDragListener( null );
                }

                else if (v==box75 && textview_random.getTag()=="75" ){
                    textview_rhenium.setVisibility( VISIBLE);
                    Integer iInteger = new Integer(R.string.rhenium);
                    remove_el_from_db(iInteger);
                    v.setBackgroundResource(R.drawable.list_correct_unknown );
                    AnimationDrawable corAnimation = (AnimationDrawable) v.getBackground();
                   corAnimation.start();

                    box75.setOnDragListener( null );
                    box17.setOnDragListener( null );


                    bcg_rectangle_random.setVisibility( INVISIBLE );
                    rectangle_random.setVisibility( INVISIBLE );
                    bcg_rectangle_hint.setVisibility( VISIBLE );
                    rectangle_hint.setVisibility( VISIBLE );

                    hint_button.setBackgroundResource( R.drawable.errbox );


                    hint_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hint_button.setBackgroundResource( R.drawable.box_display );
                            if (hint_display.getVisibility()== INVISIBLE){
                                hint_display.setVisibility( VISIBLE );
                                hint_display.bringToFront();
                                chemist.setBackgroundResource(R.drawable.livre_list);
                                AnimationDrawable livAnimation = (AnimationDrawable) chemist.getBackground();
                                livAnimation.start();
                                //animation
                            }
                            else if (hint_display.getVisibility()==View.VISIBLE){
                                hint_display.setVisibility( INVISIBLE );
                                chemist.clearAnimation();
                                chemist.setBackgroundResource( R.drawable.jumo1 );
                                //animation
                            }
                            else
                                hint_display.setVisibility( INVISIBLE );


                            bcg_rectangle_hint.setVisibility( INVISIBLE );
                            rectangle_hint.setVisibility( INVISIBLE );

                            mHandler.postDelayed(new Runnable() {
                                public void run() {
                                    place_button.setBackgroundResource( R.drawable.errbox );
                                    bcg_rectangle_place.setVisibility( VISIBLE );
                                    bcg_rectangle_place.bringToFront();
                                    rectangle_place.setVisibility( VISIBLE );
                                    rectangle_place.bringToFront();
                                }
                            }, 1000);

                            place_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    place_button.setBackgroundResource( R.drawable.box_display );
                                    hint_display.setVisibility( INVISIBLE );
                                    chemist.clearAnimation();
                                    chemist.setBackgroundResource( R.drawable.jumo1 );
                                    Object l=textview_random.getTag();

                                    bcg_rectangle_place.setVisibility( INVISIBLE );
                                    rectangle_place.setVisibility( INVISIBLE );

                                    if(l=="17"){
                                        textview_chlorine.setVisibility( VISIBLE );
                                        Integer iInteger = new Integer(R.string.chlorine);
                                        remove_el_from_db (iInteger);
                                        box17.setOnDragListener( null );
                                        box0.setVisibility( INVISIBLE );
                                        hint_button.setClickable( false );
                                        box17.setBackgroundResource(R.drawable.place_list_halogens );
                                        AnimationDrawable placeAnimation = (AnimationDrawable) box17.getBackground();
                                        placeAnimation.start();
                                        time.setText( "00" );

                                    }

                                    if(l=="75"){
                                        textview_rhenium.setVisibility( VISIBLE );
                                        Integer iInteger = new Integer(R.string.rhenium);
                                        remove_el_from_db (iInteger);
                                        box75.setBackgroundResource(R.drawable.place_list_tmetals );
                                        box75.setOnDragListener( null );
                                        box75.setBackgroundResource(R.drawable.place_list_halogens );
                                        AnimationDrawable placeAnimation = (AnimationDrawable) box75.getBackground();
                                        placeAnimation.start();
                                        time.setText( "00" );
                                    }

                                    mHandler.postDelayed(new Runnable() {
                                        public void run() {
                                            opaque.setVisibility( VISIBLE );
                                            end_tuto_display.setVisibility( VISIBLE );
                                            bcg_rectangle_place.setVisibility( INVISIBLE );
                                            rectangle_place.setVisibility( INVISIBLE );
                                        }
                                    }, 1000);



                                    playagain.setOnClickListener( new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            recreate();
                                        }
                                    } );

                                    gotogame.setOnClickListener( new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent2 = new Intent( Instructions.this, ScrollviewLevels.class );
                                            startActivity( intent2 );
                                            Instructions.this.finish();
                                            recreate();
                                        }
                                    } );




                                }});

                        }});





                    hint_display_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hint_display.setVisibility( INVISIBLE );
                        }});
                }


                break;
        }

        return true;


    }


    //****************************************
    //Fonctions
    //***************************************


    Elements2 element = new Elements2 ();


    protected void set_tag_to_elements (int n){

        if(n==R.string.chlorine){
            textview_random.setTag( "17" );
            hint_display_text.setText( R.string.chlorine_hint );
            random_info_display_text.setText( R.string.chlorine_info );

        }

        if(n==R.string.rhenium){
            textview_random.setTag( "75" );
            hint_display_text.setText( R.string.rhenium_hint );
            random_info_display_text.setText( R.string.rhenium_info );
        }

    }

    // Just remove an element from database
    protected void remove_el_from_db (Integer integer){
        chemist.setBackgroundResource(R.drawable.jump_list);
        AnimationDrawable shAnimation = (AnimationDrawable) chemist.getBackground();
        shAnimation.start();
        shAnimation.setOneShot( true );

        if (element.el.size()>=1){
            element.el.remove( integer );
            ArrayList <Integer> zz = element.getEl();
            //int w = new Random().nextInt(zz.size());
            //Integer xx= zz.get( w );
            int k = R.string.chlorine;
            textview_random.setText( k );
            set_tag_to_elements( k );
            element.setEl( zz );
        }
        else {
            box0.setVisibility( INVISIBLE );
            time.setText( "00" );
            hint_button.setClickable( false );
        }

    }








}



