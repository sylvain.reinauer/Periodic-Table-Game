package com.periodictable.chemgames.periodictablememorizer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;


public class RecyclerViewLevelsKnownElement extends RecyclerViewLevelsUnknownElement {
    private ArrayList<Item_recycler> data;
    ItemArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate( savedInstanceState );

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        initializeData();
        adapter = new ItemArrayAdapter(this, data, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = null;
                int postId = data.get(position).getItemName();
                // do what ever you want to do with it


                switch (postId){
                    case R.string.alkalimetals:
                        Intent alkali_metals=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        alkali_metals.putExtra( "type", "Alkaline" );
                        alkali_metals.putExtra( "level_type", "Group" );
                        startActivity (alkali_metals);
                        break;
                    case R.string.alkaliearthmetals:
                        Intent alkaline_earth_metals=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        alkaline_earth_metals.putExtra( "type", "Alk_earth" );
                        alkaline_earth_metals.putExtra( "level_type", "Group" );
                        startActivity (alkaline_earth_metals);
                        break;

                    case R.string.tmetals:
                        Intent t_metals=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        t_metals.putExtra( "type", "Transition metals" );
                        t_metals.putExtra( "level_type", "Group" );
                        startActivity (t_metals);
                        break;

                    case R.string.posttm:
                        Intent posttm=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        posttm.putExtra( "type", "Post-transition metals" );
                        posttm.putExtra( "level_type", "Group" );
                        startActivity (posttm);
                        break;

                    case R.string.metalloids:
                        Intent metalloids=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        metalloids.putExtra( "type", "Metalloids" );
                        metalloids.putExtra( "level_type", "Group" );
                        startActivity (metalloids);
                        break;

                    case R.string.nmetals:
                        Intent nonmetals=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        nonmetals.putExtra( "type", "Non-metals" );
                        nonmetals.putExtra( "level_type", "Group" );
                        startActivity (nonmetals);
                        break;

                    case R.string.halogens:
                        Intent halogens=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        halogens.putExtra( "type", "Halogens" );
                        halogens.putExtra( "level_type", "Group" );
                        startActivity (halogens);
                        break;

                    case R.string.ngases:
                        Intent nobleg=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        nobleg.putExtra( "type", "Noble gases" );
                        nobleg.putExtra( "level_type", "Group" );
                        startActivity (nobleg);
                        break;

                    case R.string.lanthanides:
                        Intent lanthanides=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        lanthanides.putExtra( "type", "Lanthanides" );
                        lanthanides.putExtra( "level_type", "Group" );
                        startActivity (lanthanides);
                        break;

                    case R.string.actinides:
                        Intent actinides=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        actinides.putExtra( "type", "Actinides" );
                        actinides.putExtra( "level_type", "Group" );
                        startActivity (actinides);
                        break;

                    case R.string.random1:
                        Intent random1=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        random1.putExtra( "type", "Random1" );
                        random1.putExtra( "level_type", "Random" );
                        startActivity (random1);
                        break;

                    case R.string.random2:
                        Intent random2=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        random2.putExtra( "type", "Random2" );
                        random2.putExtra( "level_type", "Random" );
                        startActivity (random2);
                        break;

                    case R.string.el136:
                        Intent els_1_36=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        els_1_36.putExtra( "type", "Elements_1_36" );
                        els_1_36.putExtra( "level_type", "Element_list" );
                        startActivity (els_1_36);
                        break;

                    case R.string.el156:
                        Intent els_1_56=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        els_1_56.putExtra( "type", "Elements_1_56" );
                        els_1_56.putExtra( "level_type", "Element_list" );
                        startActivity (els_1_56);
                        break;

                    case R.string.whtable:
                        Intent iwh=  new Intent( RecyclerViewLevelsKnownElement.this, Levels_opt.class  );
                        iwh.putExtra( "level_type", "Whole" );
                        startActivity (iwh);
                        break;
                    default:
                        intent = null;
                        break;
                }
                if (intent != null) {
                    //startActivity( intent );
                }
            }
        });
        recyclerView.setAdapter(adapter);

    }

    private void initializeData() {
        data = new ArrayList<>();
        data.add( new Item_recycler(  R.string.alkalimetals, R.string.malkalimetals, R.drawable.ic_periodic_levels_alkaline, 0 ) );
        data.add( new Item_recycler( R.string.alkaliearthmetals, R.string.malkaliearthmetals, R.drawable.ic_periodic_levels_alk_e, 0 ) );
        data.add( new Item_recycler( R.string.tmetals, R.string.mtmetals, R.drawable.ic_periodic_levels_tmetals, 0 ) );
        data.add( new Item_recycler( R.string.posttm, R.string.mposttm, R.drawable.ic_periodic_levels_posttm, 0 ) );
        data.add( new Item_recycler( R.string.metalloids, R.string.mmetalloids, R.drawable.ic_periodic_levels_metalloids, 0 ) );
        data.add( new Item_recycler( R.string.nmetals, R.string.mnmetals, R.drawable.ic_periodic_levels_nonmet, 0 ) );
        data.add( new Item_recycler( R.string.halogens, R.string.mhalogens, R.drawable.ic_periodic_levels_halo, 0 ) );
        data.add( new Item_recycler( R.string.ngases, R.string.mngases, R.drawable.ic_periodic_levels_nobleg, 0 ) );
        data.add( new Item_recycler( R.string.lanthanides, R.string.mlanthanides, R.drawable.ic_periodic_levels_lanth, 0 ) );
        data.add( new Item_recycler( R.string.actinides, R.string.mactinides, R.drawable.ic_periodic_levels_act, 0 ) );
        data.add( new Item_recycler( R.string.random1, R.string.mrandom1, R.drawable.ic_periodic_levels_rand1, ad_flag11 ) );
        data.add( new Item_recycler( R.string.random2, R.string.mrandom2, R.drawable.ic_periodic_levels_rand2, ad_flag12 ) );
        data.add( new Item_recycler( R.string.el136, R.string.mel136, R.drawable.ic_periodic_levels_36, ad_flag13 ) );
        data.add( new Item_recycler( R.string.el156, R.string.mel156, R.drawable.ic_periodic_levels_56, ad_flag14 ) );
        data.add( new Item_recycler( R.string.whtable, R.string.mwhtable, R.drawable.ic_periodic_levels_whole, ad_flag15 ) );


    }



    public boolean check_ad_state (int level_state, int level_mask){
            return true;
    }


    }



