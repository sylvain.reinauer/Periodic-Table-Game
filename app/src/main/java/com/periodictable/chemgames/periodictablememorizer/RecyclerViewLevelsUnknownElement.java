package com.periodictable.chemgames.periodictablememorizer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;

public class RecyclerViewLevelsUnknownElement extends AppCompatActivity implements RewardedVideoAdListener {

    //UI
    @BindView(R.id.leveltitle) TextView leveltitle;
    @BindView(R.id.textview_addialog) TextView textView_addialog;
    @BindView(R.id.ad_dialog_no) Button ad_button_no;
    @BindView(R.id.ad_dialog_yes) Button ad_button_yes;
    @BindView(R.id.ad_dialog) ConstraintLayout ad_dialog;


    private ArrayList<Item_recycler> data;
    ItemArrayAdapter adapter;


    //For shared preferences_buy_ad
    int level_label;
    private static final int LEVEL15_MASK = 0b10000;
    private static final int LEVEL14_MASK = 0b01000;
    private static final int LEVEL13_MASK = 0b00100;
    private static final int LEVEL12_MASK = 0b00010;
    private static final int LEVEL11_MASK = 0b00001;
    int ad_state;
    int ad_flag11;
    int ad_flag12;
    int ad_flag13;
    int ad_flag14;
    int ad_flag15;
    boolean ad_check;
    boolean ad_check11;
    boolean ad_check12;
    boolean ad_check13;
    boolean ad_check14;
    boolean ad_check15;
    SharedPreferences sharedPreferences;

    //Banner
    private AdView mAdView;
    //Ad reward
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN );

        setRequestedOrientation( SCREEN_ORIENTATION_LANDSCAPE );
        setContentView(R.layout.activity_list_recycler);
        ButterKnife.bind( this );


        //Ads
        MobileAds.initialize( this, "ca-app-pub-2987907818101616~4282552993" );

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance( this );
        mRewardedVideoAd.setRewardedVideoAdListener( this );
        loadRewardedVideoAd();

        mAdView = findViewById( R.id.adView );
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd( adRequest );


        //Shared preferences

        sharedPreferences = getSharedPreferences( "myPrefs", MODE_PRIVATE );
        //re-initialize shared preferences for calibration:
        //sharedPreferences.edit().putInt("ad_state", 0).apply();
        ad_state = sharedPreferences.getInt( "ad_state", 0 );

        ad_check11 = check_ad_state( ad_state, LEVEL11_MASK );
        if (ad_check11) {
            ad_flag11 = 0;
        } else ad_flag11 = R.drawable.ic_ad_icon;
        ad_check12 = check_ad_state( ad_state, LEVEL12_MASK );
        if (ad_check12) {
            ad_flag12 = 0;
        } else ad_flag12 = R.drawable.ic_ad_icon;
        ad_check13 = check_ad_state( ad_state, LEVEL13_MASK );
        if (ad_check13) {
            ad_flag13 = 0;
        } else ad_flag13 = R.drawable.ic_ad_icon;
        ad_check14 = check_ad_state( ad_state, LEVEL14_MASK );
        if (ad_check14) {
            ad_flag14 = 0;
        } else ad_flag14 = R.drawable.ic_ad_icon;
        ad_check15 = check_ad_state( ad_state, LEVEL15_MASK );
        if (ad_check15) {
            ad_flag15 = 0;
        } else ad_flag15 = R.drawable.ic_ad_icon;

        // RECYCLER VIEW
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // this is data for recycler view
        initializeData();
        // 3. create an adapter with interface
        adapter = new ItemArrayAdapter(this, data, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = null;
                int postId = data.get(position).getItemName();
                // do what ever you want to do with it

                ad_button_no.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        textView_addialog.setText( R.string.to_unlock_this_level );
                        ad_dialog.setVisibility( View.INVISIBLE );

                    }
                } );

                ad_button_yes.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (mRewardedVideoAd.isLoaded()) {
                            textView_addialog.setText( R.string.to_unlock_this_level );
                            ad_dialog.setVisibility( View.INVISIBLE );
                            mRewardedVideoAd.show();

                        } else
                            textView_addialog.setText( R.string.ad_dialog_ad_not_ready );
                        loadRewardedVideoAd();
                    }
                } );



                switch (postId){
                    case R.string.alkalimetals:
                        Intent alkali_metals=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        alkali_metals.putExtra( "type", "Alkaline" );
                        alkali_metals.putExtra( "level_type", "Group" );
                        startActivity (alkali_metals);
                        break;
                    case R.string.alkaliearthmetals:
                        Intent alkaline_earth_metals=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        alkaline_earth_metals.putExtra( "type", "Alk_earth" );
                        alkaline_earth_metals.putExtra( "level_type", "Group" );
                        startActivity (alkaline_earth_metals);
                        break;

                    case R.string.tmetals:
                        Intent t_metals=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        t_metals.putExtra( "type", "Transition metals" );
                        t_metals.putExtra( "level_type", "Group" );
                        startActivity (t_metals);
                        break;

                    case R.string.posttm:
                        Intent posttm=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        posttm.putExtra( "type", "Post-transition metals" );
                        posttm.putExtra( "level_type", "Group" );
                        startActivity (posttm);
                        break;

                    case R.string.metalloids:
                        Intent metalloids=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        metalloids.putExtra( "type", "Metalloids" );
                        metalloids.putExtra( "level_type", "Group" );
                        startActivity (metalloids);
                        break;

                    case R.string.nmetals:
                        Intent nonmetals=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        nonmetals.putExtra( "type", "Non-metals" );
                        nonmetals.putExtra( "level_type", "Group" );
                        startActivity (nonmetals);
                        break;

                    case R.string.halogens:
                        Intent halogens=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        halogens.putExtra( "type", "Halogens" );
                        halogens.putExtra( "level_type", "Group" );
                        startActivity (halogens);
                        break;

                    case R.string.ngases:
                        Intent nobleg=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        nobleg.putExtra( "type", "Noble gases" );
                        nobleg.putExtra( "level_type", "Group" );
                        startActivity (nobleg);
                        break;

                    case R.string.lanthanides:
                        Intent lanthanides=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        lanthanides.putExtra( "type", "Lanthanides" );
                        lanthanides.putExtra( "level_type", "Group" );
                        startActivity (lanthanides);
                        break;

                    case R.string.actinides:
                        Intent actinides=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                        actinides.putExtra( "type", "Actinides" );
                        actinides.putExtra( "level_type", "Group" );
                        startActivity (actinides);
                        break;

                    case R.string.random1:
                        level_label = 11;
                        ad_check = check_ad_state( ad_state, LEVEL11_MASK );
                        if (ad_check == true) {
                            Intent random1=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                            random1.putExtra( "type", "Random1" );
                            random1.putExtra( "level_type", "Random" );
                            startActivity (random1);
                        } else {
                            ad_dialog.setVisibility( View.VISIBLE );
                        }
                        break;

                    case R.string.random2:
                        level_label = 12;
                        ad_check = check_ad_state( ad_state, LEVEL12_MASK );
                        if (ad_check == true) {
                            Intent random2=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                            random2.putExtra( "type", "Random2" );
                            random2.putExtra( "level_type", "Random" );
                            startActivity (random2);
                        } else {
                            ad_dialog.setVisibility( View.VISIBLE );
                        }
                        break;

                    case R.string.el136:
                        level_label = 13;
                        ad_check = check_ad_state( ad_state, LEVEL13_MASK );
                        if (ad_check == true) {
                            Intent els_1_36=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                            els_1_36.putExtra( "type", "Elements_1_36" );
                            els_1_36.putExtra( "level_type", "Element_list" );
                            startActivity (els_1_36);

                        } else {
                            ad_dialog.setVisibility( View.VISIBLE );
                        }
                        break;

                    case R.string.el156:
                        level_label = 14;
                        ad_check = check_ad_state( ad_state, LEVEL14_MASK );
                        if (ad_check == true) {
                            Intent els_1_56=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                            els_1_56.putExtra( "type", "Elements_1_56" );
                            els_1_56.putExtra( "level_type", "Element_list" );
                            startActivity (els_1_56);

                        } else {
                            ad_dialog.setVisibility( View.VISIBLE );
                        }
                        break;

                    case R.string.whtable:
                        level_label = 15;
                        ad_check = check_ad_state( ad_state, LEVEL15_MASK );
                        if (ad_check == true) {
                            Intent iwh=  new Intent( RecyclerViewLevelsUnknownElement.this, Levels_unk_opt.class  );
                            iwh.putExtra( "level_type", "Whole" );
                            startActivity (iwh);

                        } else {
                            ad_dialog.setVisibility( View.VISIBLE );
                        }
                        break;
                    default:
                        intent = null;
                        break;
                }
                if (intent != null) {
                    //startActivity( intent );
                }
            }
        });
        recyclerView.setAdapter(adapter);

    }






    //METHODS



    //Rewarded ad methods


    // Test ad
    private void loadRewardedVideoAd() {
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd = null;
            mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance( this );
            mRewardedVideoAd.setRewardedVideoAdListener( this );

            //mRewardedVideoAd.loadAd( "ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build() );
        }
    }


    @Override
    public void onRewardedVideoAdLeftApplication() {
    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewarded(RewardItem reward) {

        // Reward the user:
        //update shared preferences
        switch (level_label) {
            case 11:
                save_ad_state( ad_state );
                break;
            case 12:
                save_ad_state( ad_state );
                break;
            case 13:
                save_ad_state( ad_state );
                break;
            case 14:
                save_ad_state( ad_state );
                break;
            case 15:
                save_ad_state( ad_state );
                break;
        }
        this.recreate();
        loadRewardedVideoAd();
    }


    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
    }


    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoCompleted() {
        loadRewardedVideoAd();
    }


    //Ad_checking methods

    public boolean check_ad_state(int level_state, int level_mask) {
        if ((level_state & level_mask) == level_mask) {
            return true;
        } else return false;
    }

    public void save_ad_state(int level_state) {


        switch (level_label) {
            case 11:
                level_state = level_state | LEVEL11_MASK;
                break;
            case 12:
                level_state = level_state | LEVEL12_MASK;
                break;
            case 13:
                level_state = level_state | LEVEL13_MASK;
                break;
            case 14:
                level_state = level_state | LEVEL14_MASK;
                break;
            case 15:
                level_state = level_state | LEVEL15_MASK;
                break;

        }
        sharedPreferences.edit().putInt( "ad_state", level_state ).apply();
        Toast.makeText( this, "Level unlocked", Toast.LENGTH_SHORT ).show();


    }
//Lifecycle methods


    @Override
    public void onResume() {
        mRewardedVideoAd.resume( this );
        super.onResume();
        loadRewardedVideoAd();
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause( this );
        super.onPause();
        loadRewardedVideoAd();
    }


        //RecyclerView methods


        private void initializeData() {
        data = new ArrayList<>();
                data.add( new Item_recycler(  R.string.alkalimetals, R.string.malkalimetals, R.drawable.ic_periodic_levels_alkaline, 0 ) );
                data.add( new Item_recycler( R.string.alkaliearthmetals, R.string.malkaliearthmetals, R.drawable.ic_periodic_levels_alk_e, 0 ) );
                data.add( new Item_recycler( R.string.tmetals, R.string.mtmetals, R.drawable.ic_periodic_levels_tmetals, 0 ) );
                data.add( new Item_recycler( R.string.posttm, R.string.mposttm, R.drawable.ic_periodic_levels_posttm, 0 ) );
                data.add( new Item_recycler( R.string.metalloids, R.string.mmetalloids, R.drawable.ic_periodic_levels_metalloids, 0 ) );
                data.add( new Item_recycler( R.string.nmetals, R.string.mnmetals, R.drawable.ic_periodic_levels_nonmet, 0 ) );
                data.add( new Item_recycler( R.string.halogens, R.string.mhalogens, R.drawable.ic_periodic_levels_halo, 0 ) );
                data.add( new Item_recycler( R.string.ngases, R.string.mngases, R.drawable.ic_periodic_levels_nobleg, 0 ) );
                data.add( new Item_recycler( R.string.lanthanides, R.string.mlanthanides, R.drawable.ic_periodic_levels_lanth, 0 ) );
                data.add( new Item_recycler( R.string.actinides, R.string.mactinides, R.drawable.ic_periodic_levels_act, 0 ) );
                data.add( new Item_recycler( R.string.random1, R.string.mrandom1, R.drawable.ic_periodic_levels_rand1, ad_flag11 ) );
                data.add( new Item_recycler( R.string.random2, R.string.mrandom2, R.drawable.ic_periodic_levels_rand2, ad_flag12 ) );
                data.add( new Item_recycler( R.string.el136, R.string.mel136, R.drawable.ic_periodic_levels_36, ad_flag13 ) );
                data.add( new Item_recycler( R.string.el156, R.string.mel156, R.drawable.ic_periodic_levels_56, ad_flag14 ) );
                data.add( new Item_recycler( R.string.whtable, R.string.mwhtable, R.drawable.ic_periodic_levels_whole, ad_flag15 ) );


    }

}
