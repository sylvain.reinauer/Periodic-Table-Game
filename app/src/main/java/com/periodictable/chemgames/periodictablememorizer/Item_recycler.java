package com.periodictable.chemgames.periodictablememorizer;

public class Item_recycler {
    private int itemName;
    private int itemDescription;
    private int itemImage;
    private int itemFlag;

    public Item_recycler(int name, int description, int image, int flag){

        this.itemName = name;
        this.itemDescription = description;
        this.itemImage = image;
        this.itemFlag = flag;

    }


    public int getItemName() {
        return itemName;
    }

    public int getItemDescription() {
        return itemDescription;
    }

    public int getItemImage() {
        return itemImage;
    }

    public int getItemFlag() {
        return itemFlag;
    }


}