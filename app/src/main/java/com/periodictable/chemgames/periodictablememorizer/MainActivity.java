package com.periodictable.chemgames.periodictablememorizer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.periodictable.chemgames.periodictablememorizer.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;

//TODO: gérer la sortie correcte

public class MainActivity extends AppCompatActivity {
    AnimationDrawable shAnimation;

    @BindView(R.id.scientistanim) ImageView shtittleimage;
    @BindView(R.id.play) ConstraintLayout play;
    @BindView(R.id.learn) ConstraintLayout learn;
    @BindView(R.id.tutorial) ConstraintLayout tutorial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(SCREEN_ORIENTATION_LANDSCAPE);
        setContentView( R.layout.activity_main_c );
        ButterKnife.bind(this);

        //Animation

        shtittleimage.setBackgroundResource(R.drawable.startscreen_list);
        shAnimation = (AnimationDrawable) shtittleimage.getBackground();
        shAnimation.start();
        }


    //Boutons
    @OnClick({R.id.play, R.id.learn, R.id.tutorial})
    public void startAction (View v) {
        switch(v.getId()){
            case R.id.play:
                shAnimation.stop();
                startActivity (new Intent( MainActivity.this, ScrollviewLevels.class ));
                break;
            case R.id.learn:
                shAnimation.stop();
                startActivity (new Intent( MainActivity.this, Info_page.class));
                break;
            case R.id.tutorial:
                shAnimation.stop();
                startActivity (new Intent( MainActivity.this, Instructions.class));
                break;
        }
    }

    //Lifecycle methods

    @Override
    protected void onRestart() {
        super.onRestart();
        shAnimation.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shAnimation.start();
    }
}
