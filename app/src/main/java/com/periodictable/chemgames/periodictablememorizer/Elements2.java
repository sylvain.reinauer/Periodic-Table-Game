package com.periodictable.chemgames.periodictablememorizer;

import java.util.ArrayList;

import com.periodictable.chemgames.periodictablememorizer.R;

public class Elements2 {

    public ArrayList<Integer> el;

    public Elements2( ) {

        // Make member variables private
        el = new ArrayList <Integer> (250);

        el.add ( R.string.chlorine  );

        el.add (R.string.rhenium);


    }

    public ArrayList<Integer> getEl() {
        return el;
    }

    public void setEl(ArrayList<Integer> el) {
        this.el = el;
    }


}


