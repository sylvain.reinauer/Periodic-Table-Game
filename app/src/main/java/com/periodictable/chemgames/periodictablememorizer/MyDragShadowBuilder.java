package com.periodictable.chemgames.periodictablememorizer;

/**
 * Created by olga on 14.05.18.
 */
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

/**
 * Created by olga on 05.04.18.
 */

public  class MyDragShadowBuilder extends View.DragShadowBuilder {

    View v;

    public MyDragShadowBuilder(View v) {
       super(v);
       this.v=v;

    }
    //  @Override
    @SuppressLint("ResourceAsColor")
    public void onDrawShadow(Canvas canvas) {
        super.onDrawShadow(canvas);

    /*Modify canvas if you want to show some custom view that you want
      to animate, that you can check by putting a condition passed over
      constructor.

     Here I'm taking the same view*/



        Paint strokePaint = new Paint();
        strokePaint.setAntiAlias(true);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(4f);
        strokePaint.setColor(R.color.dkgray);

        Paint fillPaint = new Paint();
        fillPaint.setAntiAlias(true);
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setColor(Color.rgb(255,255,255));
        //fillPaint.setAlpha(255);

        Paint linexpaint = new Paint();
        linexpaint.setColor(R.color.dkgray);
        linexpaint.setStyle(Paint.Style.STROKE);
        linexpaint.setStrokeWidth(4f);
        linexpaint.setAntiAlias(true);
        int offset = v.getWidth()/2;

        Paint lineypaint = new Paint();
        lineypaint.setColor(R.color.dkgray);
        lineypaint.setStyle(Paint.Style.STROKE);
        lineypaint.setStrokeWidth(4f);
        lineypaint.setAntiAlias(true);
        int yoffset = v.getHeight()/2;

        canvas.drawCircle(v.getWidth()/2, v.getHeight()/2, v.getHeight()/3, strokePaint);
        canvas.drawCircle(v.getWidth()/2, v.getHeight()/2, v.getHeight()/3, fillPaint);
        canvas.drawLine( -offset, v.getHeight() / 2, v.getWidth() + offset, v.getHeight() / 2,linexpaint );
        canvas.drawLine( v.getWidth()/2, -yoffset, v.getWidth()/2, v.getHeight() + yoffset,lineypaint );



    }
    @Override
    public void onProvideShadowMetrics(Point shadowSize, Point touchPoint) {
/*Modify x,y of shadowSize to change the shadow view
   according to your requirements. Here I'm taking the same view width and height*/
        shadowSize.set(v.getWidth(),v.getHeight());
/*Modify x,y of touchPoint to change the touch for the view
 as per your needs. You may pass your x,y position of finger
 to achieve your results. Here I'm taking the lower end point of view*/
        touchPoint.set(v.getWidth()/2, v.getHeight()/2);

    }



}