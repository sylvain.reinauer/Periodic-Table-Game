package com.periodictable.chemgames.periodictablememorizer;

import android.view.View;

public interface CustomItemClickListener {

    public void onItemClick(View v, int position);
}
