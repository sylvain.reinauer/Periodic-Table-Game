package com.periodictable.chemgames.periodictablememorizer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.view.DragEvent.ACTION_DROP;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class Levels_unk_opt extends Levels_opt {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //restore any saved state
        super.onCreate( savedInstanceState );
        box0.setBackgroundResource( R.drawable.ic_invisible_element );
        textviewRandom.setVisibility( View.INVISIBLE );

        Intent i = getIntent();
        s = i.getStringExtra( "type" );
        level_type = i.getStringExtra( "level_type" );

        box0.setOnTouchListener(this);
        box1.setOnDragListener(this);
        box2.setOnDragListener(this);
        box3.setOnDragListener(this);
        box4.setOnDragListener(this);
        box5.setOnDragListener(this);
        box6.setOnDragListener(this);
        box7.setOnDragListener(this);
        box8.setOnDragListener(this);
        box9.setOnDragListener(this);
        box10.setOnDragListener(this);
        box11.setOnDragListener(this);
        box12.setOnDragListener(this);
        box13.setOnDragListener(this);
        box14.setOnDragListener(this);
        box15.setOnDragListener(this);
        box16.setOnDragListener(this);
        box17.setOnDragListener(this);
        box18.setOnDragListener(this);
        box19.setOnDragListener(this);
        box20.setOnDragListener(this);
        box21.setOnDragListener(this);
        box22.setOnDragListener(this);
        box23.setOnDragListener(this);
        box24.setOnDragListener(this);
        box25.setOnDragListener(this);
        box26.setOnDragListener(this);
        box27.setOnDragListener(this);
        box28.setOnDragListener(this);
        box29.setOnDragListener(this);
        box30.setOnDragListener(this);
        box31.setOnDragListener(this);
        box32.setOnDragListener(this);
        box33.setOnDragListener(this);
        box34.setOnDragListener(this);
        box35.setOnDragListener(this);
        box36.setOnDragListener(this);
        box37.setOnDragListener(this);
        box38.setOnDragListener(this);
        box39.setOnDragListener(this);
        box40.setOnDragListener(this);
        box41.setOnDragListener(this);
        box42.setOnDragListener(this);
        box43.setOnDragListener(this);
        box44.setOnDragListener(this);
        box45.setOnDragListener(this);
        box46.setOnDragListener(this);
        box47.setOnDragListener(this);
        box48.setOnDragListener(this);
        box49.setOnDragListener(this);
        box50.setOnDragListener(this);
        box51.setOnDragListener(this);
        box52.setOnDragListener(this);
        box53.setOnDragListener(this);
        box54.setOnDragListener(this);
        box55.setOnDragListener(this);
        box56.setOnDragListener(this);
        box57.setOnDragListener(this);
        box58.setOnDragListener(this);
        box59.setOnDragListener(this);
        box60.setOnDragListener(this);
        box61.setOnDragListener(this);
        box62.setOnDragListener(this);
        box63.setOnDragListener(this);
        box64.setOnDragListener(this);
        box65.setOnDragListener(this);
        box66.setOnDragListener(this);
        box67.setOnDragListener(this);
        box68.setOnDragListener(this);
        box69.setOnDragListener(this);
        box70.setOnDragListener(this);
        box71.setOnDragListener(this);
        box72.setOnDragListener(this);
        box73.setOnDragListener(this);
        box74.setOnDragListener(this);
        box75.setOnDragListener(this);
        box76.setOnDragListener(this);
        box77.setOnDragListener(this);
        box78.setOnDragListener(this);
        box79.setOnDragListener(this);
        box80.setOnDragListener(this);
        box81.setOnDragListener(this);
        box82.setOnDragListener(this);
        box83.setOnDragListener(this);
        box84.setOnDragListener(this);
        box85.setOnDragListener(this);
        box86.setOnDragListener(this);
        box87.setOnDragListener(this);
        box88.setOnDragListener(this);
        box89.setOnDragListener(this);
        box90.setOnDragListener(this);
        box91.setOnDragListener(this);
        box92.setOnDragListener(this);
        box93.setOnDragListener(this);
        box94.setOnDragListener(this);
        box95.setOnDragListener(this);
        box96.setOnDragListener(this);
        box97.setOnDragListener(this);
        box98.setOnDragListener(this);
        box99.setOnDragListener(this);
        box100.setOnDragListener(this);
        box101.setOnDragListener(this);
        box102.setOnDragListener(this);
        box103.setOnDragListener(this);
        box104.setOnDragListener(this);
        box105.setOnDragListener(this);
        box106.setOnDragListener(this);
        box107.setOnDragListener(this);
        box108.setOnDragListener(this);
        box109.setOnDragListener(this);
        box110.setOnDragListener(this);
        box111.setOnDragListener(this);
        box112.setOnDragListener(this);
        box113.setOnDragListener(this);
        box114.setOnDragListener(this);
        box115.setOnDragListener(this);
        box116.setOnDragListener(this);
        box117.setOnDragListener(this);
        box118.setOnDragListener(this);


        l = new ArrayList<Element>();
        Element e1= new Element (R.string.hydrogen,R.string.hydrogen_hintu, R.string.hydrogen_info, R.string.hydrogen_name, R.string.type_hydrogen, box1, textviewHydrogen, R.string.hydrogen_el_config, 1.008f, 13.598f, 2.20f, 13.99f, 20.271f, 25 );
        Element e2= new Element (R.string.helium,R.string.helium_hintu, R.string.helium_info, R.string.helium_name, R.string.ngases, box2, textviewHelium, R.string.helium_el_config, 4.003f, 24.587f, 0, 0.95f, 4.22f, 120 );
        Element e3= new Element (R.string.lithium,R.string.lithium_hintu, R.string.lithium_info, R.string.lithium_name, R.string.alkaline, box3, textviewLithium, R.string.lithium_el_config, 6.94f, 5.392f, 0.98f, 453.69f, 1560.0f, 145 );
        Element e4= new Element (R.string.berillium, R.string.berillium_hintu, R.string.berillium_info, R.string.berillium_name, R.string.alk_earth, box4, textviewBerillium, R.string.berillium_el_config, 9.012f, 9.323f, 1.57f, 1560.0f, 2742.0f, 105 );
        Element e5= new Element (R.string.boron,R.string.boron_hintu, R.string.boron_info, R.string.boron_name, R.string.metalloids, box5, textviewBoron, R.string.boron_el_config, 10.81f, 8.298f, 2.04f, 2349.0f, 4200.0f, 85 );
        Element e6= new Element (R.string.carbon,R.string.carbon_hintu, R.string.carbon_info, R.string.carbon_name, R.string.nmetals, box6, textviewCarbon, R.string.carbon_el_config, 12.011f, 11.26f, 2.55f, 3800.0f, 4300.0f, 70 );
        Element e7= new Element (R.string.nitrogen,R.string.nitrogen_hintu, R.string.nitrogen_info, R.string.nitrogen_name, R.string.nmetals, box7, textviewNitrogen, R.string.nitrogen_el_config, 14.007f, 14.534f, 3.04f, 63.15f, 77.36f, 65 );
        Element e8= new Element (R.string.oxygen,R.string.oxygen_hintu, R.string.oxygen_info, R.string.oxygen_name, R.string.nmetals, box8, textviewOxygen, R.string.oxygen_el_config, 15.999f, 13.618f, 3.44f, 54.36f, 90.20f, 60 );
        Element e9= new Element (R.string.fluorine,R.string.fluorine_hintu, R.string.fluorine_info, R.string.fluorine_name, R.string.halogens, box9, textviewFluorine, R.string.fluorine_el_config, 18.996f, 17.423f, 3.98f, 53.53f, 85.03f, 50 );
        Element e10= new Element (R.string.neon,R.string.neon_hintu, R.string.neon_info, R.string.neon_name, R.string.ngases, box10, textviewNeon, R.string.neon_el_config, 20.18f, 21.565f, 0, 24.56f, 27.07f, 160 );

        Element e11= new Element (R.string.sodium, R.string.sodium_hintu, R.string.sodium_info, R.string.sodium_name, R.string.alkaline, box11, textviewSodium, R.string.sodium_el_config, 22.99f, 5.139f, 0.93f, 370.87f, 1156.0f, 180  );
        Element e12= new Element (R.string.magnesium,R.string.magnesium_hintu, R.string.magnesium_info, R.string.magnesium_name, R.string.alk_earth, box12, textviewMagnesium, R.string.magnesium_el_config, 24.305f, 7.646f, 1.31f, 923.0f, 1363.0f, 150 );
        Element e13= new Element (R.string.aluminium,R.string.aluminium_hintu, R.string.aluminium_info, R.string.aluminium_name, R.string.posttm, box13, textviewAluminium, R.string.aluminium_el_config, 26.982f, 5.986f, 1.61f, 933.47f, 2792.0f, 125 );
        Element e14= new Element (R.string.silicon,R.string.silicon_hintu, R.string.silicon_info, R.string.silicon_name, R.string.metalloids, box14, textviewSilicon, R.string.silicon_el_config, 28.085f, 8.152f, 1.90f, 1687.0f, 3538.0f, 110 );
        Element e15= new Element (R.string.phosphorous,R.string.phosphorous_hintu, R.string.phosphorous_info, R.string.phosphorous_name, R.string.nmetals, box15, textviewPhosphorous, R.string.phosphorous_el_config, 30.974f, 10.487f, 2.19f, 317.30f, 550.0f, 100 );
        Element e16= new Element (R.string.sulfur,R.string.sulfur_hintu, R.string.sulfur_info, R.string.sulfur_name, R.string.nmetals, box16, textviewSulfur, R.string.sodium_el_config, 32.06f, 10.36f, 2.58f, 388.36f, 717.87f, 100 );
        Element e17= new Element (R.string.chlorine,R.string.chlorine_hintu, R.string.chlorine_info, R.string.chlorine_name, R.string.halogens, box17, textviewChlorine, R.string.chlorine_el_config, 35.45f, 12.968f, 3.16f, 171.6f, 239.11f, 100 );
        Element e18= new Element (R.string.argon,R.string.argon_hintu, R.string.argon_info, R.string.argon_name, R.string.ngases, box18, textviewArgon, R.string.argon_el_config, 39.948f, 15.76f, 0, 83.80f, 87.30f, 71 );
        Element e19= new Element (R.string.potassium, R.string.potassium_hintu, R.string.potassium_info, R.string.potassium_name, R.string.alkaline, box19, textviewPotassium, R.string.potassium_el_config, 39.098f, 4.341f, 0.82f, 336.53f, 1032.0f, 220 );
        Element e20= new Element (R.string.calcium,R.string.calcium_hintu, R.string.calcium_info, R.string.calcium_name, R.string.alk_earth, box20, textviewCalcium, R.string.calcium_el_config, 40.078f, 6.113f, 1.0f, 1115.0f, 1757.0f, 180 );

        Element e21= new Element (R.string.scandium,R.string.scandium_hintu, R.string.scandium_info, R.string.scandium_name, R.string.tmetals, box21, textviewScandium, R.string.scandium_el_config, 44.956f, 6.562f, 1.36f, 1814.0f, 3109.0f, 160 );
        Element e22= new Element (R.string.titanium,R.string.titanium_hintu, R.string.titanium_info, R.string.titanium_name, R.string.tmetals, box22, textviewTitanium, R.string.titanium_el_config, 47.867f, 6.828f, 1.54f, 1941.0f, 3560.0f, 140 );
        Element e23= new Element (R.string.vanadium,R.string.vanadium_hintu, R.string.vanadium_info, R.string.vanadium_name, R.string.tmetals, box23, textviewVanadium, R.string.vanadium_el_config, 50.942f, 6.746f, 1.63f, 2183.0f, 3680.0f, 135 );
        Element e24= new Element (R.string.chromium,R.string.chromium_hintu, R.string.chromium_info, R.string.chromium_name, R.string.tmetals, box24, textviewChromium, R.string.chromium_el_config, 51.996f, 6.767f, 1.66f, 2180.0f, 2944.0f, 140 );
        Element e25= new Element (R.string.manganese,R.string.manganese_hintu, R.string.manganese_info, R.string.manganese_name, R.string.tmetals, box25, textviewManganese, R.string.manganese_el_config, 54.938f, 7.434f, 1.55f, 1519.0f, 2334.0f, 140 );
        Element e26= new Element (R.string.iron,R.string.iron_hintu, R.string.iron_info, R.string.iron_name, R.string.tmetals, box26, textviewIron, R.string.iron_el_config, 55.845f, 7.903f, 1.83f, 1811.0f, 3134.0f, 140 );
        Element e27= new Element (R.string.cobalt,R.string.cobalt_hintu, R.string.cobalt_info, R.string.cobalt_name, R.string.tmetals, box27, textviewCobalt, R.string.cobalt_el_config, 58.993f, 7.881f, 1.88f, 1768.0f, 3200.0f, 135 );
        Element e28= new Element (R.string.nickel,R.string.nickel_hintu, R.string.nickel_info, R.string.nickel_name, R.string.tmetals, box28, textviewNickel, R.string.nickel_el_config, 58.693f, 7.64f, 1.91f, 1728.0f, 3186.0f, 135 );
        Element e29= new Element (R.string.copper,R.string.copper_hintu, R.string.copper_info, R.string.copper_name, R.string.tmetals, box29, textviewCopper, R.string.copper_el_config, 63.346f, 7.726f, 1.90f, 1357.77f, 2835.0f, 135 );
        Element e30= new Element (R.string.zinc,R.string.zinc_hintu, R.string.zinc_info, R.string.zinc_name, R.string.tmetals, box30, textviewZinc, R.string.zinc_el_config, 65.38f, 9.394f, 1.65f, 692.88f, 1180, 135 );

        Element e31= new Element (R.string.gallium,R.string.gallium_hintu, R.string.gallium_info, R.string.gallium_name, R.string.posttm, box31, textviewGallium, R.string.gallium_el_config, 69.723f, 5.999f, 1.81f, 302.91f, 2673.0f, 130 );
        Element e32= new Element (R.string.germanium,R.string.germanium_hintu, R.string.germanium_info, R.string.germanium_name, R.string.metalloids, box32, textviewGermanium, R.string.germanium_el_config, 72.63f, 7.899f, 2.01f, 1211.40f, 3106.0f, 125 );
        Element e33= new Element (R.string.arsenic,R.string.arsenic_hintu, R.string.arsenic_info, R.string.arsenic_name, R.string.metalloids, box33, textviewArsenic, R.string.arsenic_el_config, 74.922f, 9.789f, 2.18f, 1090.0f, 887.0f, 115 );
        Element e34= new Element (R.string.selenium,R.string.selenium_hintu, R.string.selenium_info, R.string.selenium_name, R.string.nmetals, box34, textviewSelenium, R.string.selenium_el_config, 78.971f, 9.752f, 2.55f, 453.0f, 958.0f, 115 );
        Element e35= new Element (R.string.bromine,R.string.bromine_hintu, R.string.bromine_info, R.string.bromine_name, R.string.halogens, box35, textviewBromine, R.string.bromine_el_config, 79.904f, 11.814f, 2.96f, 265.8f, 332.0f, 115 );
        Element e36= new Element (R.string.krypton,R.string.krypton_hintu, R.string.krypton_info, R.string.krypton_name, R.string.ngases, box36, textviewKrypton, R.string.krypton_el_config, 83.798f, 14.0f, 3.00f, 115.79f, 119.93f, 0 );
        Element e37= new Element (R.string.rubidium, R.string.rubidium_hintu, R.string.rubidium_info,R.string.rubidium_name, R.string.alkaline, box37, textviewRubidium, R.string.rubidium_el_config, 85.468f, 4.177f, 0.82f, 312.46f, 961.0f, 235);
        Element e38= new Element (R.string.strontium,R.string.stroncium_hintu, R.string.stroncium_info, R.string.strontium_name, R.string.alk_earth, box38, textviewStroncium, R.string.stroncium_el_config, 87.62f, 5.695f, 0.95f, 1050.0f, 1655.0f, 200 );
        Element e39= new Element (R.string.yttrium,R.string.yttrium_hintu, R.string.yttrium_info, R.string.ytterbium_name, R.string.tmetals, box39, textviewYttrium, R.string.yttrium_el_config, 88.906f, 6.217f, 1.22f, 1799.0f, 3609.0f, 180 );
        Element e40= new Element (R.string.zirconium,R.string.zirconium_hintu, R.string.zirconium_info, R.string.zirconium_name, R.string.tmetals, box40, textviewZirconium, R.string.zirconium_el_config, 91.224f, 6.634f, 1.33f, 2128.0f, 4682.0f, 155 );

        Element e41= new Element (R.string.niobium,R.string.niobium_hintu, R.string.niobium_info, R.string.niobium_name, R.string.tmetals, box41, textviewNiobium, R.string.niobium_el_config, 92.906f, 6.759f, 1.6f, 2750.0f, 5017.0f, 145 );
        Element e42= new Element (R.string.molybdenum,R.string.molybdenum_hintu, R.string.molybdenum_info, R.string.molybdenum_name, R.string.tmetals, box42, textviewMolybdenum, R.string.molybdenum_el_config, 95.95f, 7.092f, 2.16f, 2896.0f, 4912.0f, 145 );
        Element e43= new Element (R.string.technetium,R.string.technetium_hintu, R.string.technetium_info, R.string.technetium_name, R.string.tmetals, box43, textviewTechnetium, R.string.technetium_el_config, 98.0f, 7.119f, 1.9f, 2430.0f, 4538.0f, 135 );
        Element e44= new Element (R.string.ruthenium,R.string.ruthenium_hintu, R.string.ruthenium_info, R.string.ruthenium_name, R.string.tmetals, box44, textviewRuthenium, R.string.ruthenium_el_config, 101.07f, 7.361f, 2.2f, 2607.0f, 4423.0f, 130 );
        Element e45= new Element (R.string.rhodium,R.string.rhodium_hintu, R.string.rhodium_info, R.string.rhodium_name, R.string.tmetals, box45, textviewRhodium, R.string.rhodium_el_config, 102.906f, 7.459f, 2.28f, 2237.0f, 3968.0f, 135 );
        Element e46= new Element (R.string.palladium,R.string.palladium_hintu, R.string.palladium_info, R.string.palladium_name, R.string.tmetals, box46, textviewPalladium, R.string.palladium_el_config, 106.42f, 8.337f, 2.20f, 1828.05f, 3236.0f, 140 );
        Element e47= new Element (R.string.silver,R.string.silver_hintu, R.string.silver_info, R.string.silver_name, R.string.tmetals, box47, textviewSilver, R.string.silver_el_config, 107.868f, 7.576f, 1.93f, 1234.93f, 2435.0f, 160 );
        Element e48= new Element (R.string.cadmium,R.string.cadmium_hintu, R.string.cadmium_info, R.string.cadmium_name, R.string.tmetals, box48, textviewCadmium, R.string.cadmium_el_config, 112.414f, 8.994f, 1.69f, 594.22f, 1040.0f, 155 );
        Element e49= new Element (R.string.indium,R.string.indium_hintu, R.string.indium_info, R.string.indium_name, R.string.posttm, box49, textviewIndium, R.string.indium_el_config, 114.818f, 5.786f, 1.78f, 429.75f, 2345.0f, 155 );
        Element e50= new Element (R.string.tin,R.string.tin_hintu, R.string.tin_info, R.string.tin_name, R.string.posttm, box50, textviewTin, R.string.tin_el_config, 118.71f, 7.344f, 1.96f, 505.08f, 2875.0f, 145 );

        Element e51= new Element (R.string.antimony,R.string.antimony_hintu, R.string.antimony_info, R.string.antimony_name, R.string.metalloids, box51, textviewAntimony, R.string.antimony_el_config, 121.76f, 8.608f, 2.05f, 903.78f, 1860.0f, 145 );
        Element e52= new Element (R.string.tellurium,R.string.tellurium_hintu, R.string.tellurium_info, R.string.tellurium_name, R.string.metalloids, box52, textviewTellurium, R.string.tellurium_el_config, 127.6f, 9.01f, 2.1f, 722.66f, 1261.0f, 140 );
        Element e53= new Element (R.string.iodine,R.string.iodine_hintu, R.string.iodine_info, R.string.iodine_name, R.string.halogens, box53, textviewIodine, R.string.iodine_el_config, 126.904f, 10.451f, 2.66f, 386.85f, 457.4f, 140 );
        Element e54= new Element (R.string.xenon, R.string.xenon_hintu, R.string.xenon_info, R.string.xenon_name, R.string.ngases, box54, textviewXenon, R.string.xenon_el_config, 131.293f, 12.129f, 2.60f, 161.4f, 165.03f, 0 );
        Element e55= new Element (R.string.caesium,R.string.caesium_hintu, R.string.caesium_info, R.string.caesium_name, R.string.alkaline, box55, textviewCaesium, R.string.caesium_el_config, 132.905f, 3.894f, 0.79f, 301.59f, 944.0f, 260 );
        Element e56= new Element (R.string.barium,R.string.barium_hintu, R.string.barium_info, R.string.barium_name, R.string.alk_earth, box56, textviewBarium, R.string.barium_el_config, 137.327f, 5.212f, 0.89f, 1000.0f, 2170.0f, 215 );
        Element e57= new Element (R.string.lanthanum,R.string.lanthanum_hintu, R.string.lanthanum_info, R.string.lanthanum_name, R.string.lanthanides, box57, textviewLanthanum, R.string.lanthanum_el_config, 138.905f, 5.577f, 1.10f, 1193.0f, 3737.0f, 195 );
        Element e58= new Element (R.string.cerium,R.string.cerium_hintu, R.string.cerium_info, R.string.cerium_name, R.string.lanthanides, box58, textviewCerium, R.string.cerium_el_config, 140.116f, 5.539f, 1.12f, 1068.0f, 3716.0f, 185 );
        Element e59= new Element (R.string.praseodymium,R.string.praseodymium_hintu, R.string.praseodymium_info, R.string.praseodymium_name, R.string.lanthanides, box59, textviewPraseodymium, R.string.praseodymium_el_config, 140.908f, 5.47f, 1.13f, 1208.0f, 3793.0f, 185 );
        Element e60= new Element (R.string.neodymium,R.string.neodymium_hintu, R.string.neodymium_info, R.string.neodymium_name, R.string.lanthanides, box60, textviewNeodymium, R.string.neodymium_el_config, 144.242f, 5.525f, 1.14f, 1297.0f, 3347.0f, 185 );

        Element e61= new Element (R.string.promethium, R.string.promethium_hintu, R.string.promethium_info, R.string.promethium_name, R.string.lanthanides, box61, textviewPromethium, R.string.promethium_el_config, 145.0f, 5.577f, 1.13f, 1315.0f, 3273.0f, 185  );
        Element e62= new Element (R.string.samarium,R.string.samarium_hintu, R.string.samarium_info, R.string.samarium_name, R.string.lanthanides, box62, textviewSamarium, R.string.samarium_el_config, 150.36f, 5.644f, 1.17f, 1345.0f, 2067.0f, 185 );
        Element e63= new Element (R.string.europium,R.string.europium_hintu, R.string.europium_info, R.string.europium_name, R.string.lanthanides, box63, textviewEuropium, R.string.europium_el_config, 151.964f, 5.67f, 1.20f, 1099.0f, 1802.0f, 185 );
        Element e64= new Element (R.string.gadolinium,R.string.gadolinium_hintu, R.string.gadolinium_info, R.string.gadolinium_name, R.string.lanthanides, box64, textviewGadolinium, R.string.gadolinium_el_config, 157.25f, 6.15f, 1.20f, 1585.0f, 3546.0f, 180 );
        Element e65= new Element (R.string.terbium,R.string.terbium_hintu, R.string.terbium_info, R.string.terbium_name, R.string.lanthanides, box65, textviewTerbium, R.string.terbium_el_config, 158.925f, 5.864f, 1.20f, 1629.0f, 3503.0f, 175 );
        Element e66= new Element (R.string.dysprosium,R.string.dysprosium_hintu, R.string.dysprosium_info, R.string.dysprosium_name, R.string.lanthanides, box66, textviewDysprosium, R.string.dysprosium_el_config, 162.5f, 5.939f, 1.22f, 1680.0f, 2840.0f, 175 );
        Element e67= new Element (R.string.holmium,R.string.holmium_hintu, R.string.holmium_info, R.string.holmium_name, R.string.lanthanides, box67, textviewHolmium, R.string.holmium_el_config, 164.93f, 6.022f, 1.23f, 1734.0f, 2993.0f, 175 );
        Element e68= new Element (R.string.erbium,R.string.erbium_hintu, R.string.erbium_info, R.string.erbium_name, R.string.lanthanides, box68, textviewErbium, R.string.erbium_el_config, 167.259f, 6.108f, 1.24f, 1802.0f, 3141.0f, 175 );
        Element e69= new Element (R.string.thulium, R.string.thulium_hintu, R.string.thulium_info, R.string.thulium_name, R.string.lanthanides, box69, textviewThulium, R.string.thulium_el_config, 168.934f, 6.184f, 1.25f, 1818.0f, 2223.0f, 175 );
        Element e70= new Element (R.string.ytterbium,R.string.ytterbium_hintu, R.string.ytterbium_info, R.string.ytterbium_name, R.string.lanthanides, box70, textviewYtterbium, R.string.ytterbium_el_config, 173.045f, 6.254f, 1.10f, 1097.0f, 1469.0f, 175 );

        Element e71= new Element (R.string.lutetium,R.string.lutetium_hintu, R.string.lutetium_info, R.string.lutetium_name, R.string.lanthanides, box71, textviewLutetium, R.string.lutetium_el_config, 174.968f, 5.426f, 1.27f, 1925.0f, 3675.0f, 175 );
        Element e72= new Element (R.string.hafnium,R.string.hafnium_hintu, R.string.hafnium_info, R.string.hafnium_name, R.string.tmetals, box72, textviewHafnium, R.string.hafnium_el_config, 178.49f, 6.825f, 1.30f, 2506.0f, 4876.0f, 155 );
        Element e73= new Element (R.string.tantalum,R.string.tantalum_hintu, R.string.tantalum_info, R.string.tantalum_name, R.string.tmetals, box73, textviewTantalum, R.string.tantalum_el_config, 180.948f, 7.55f, 1.50f, 3290.0f, 5731.0f, 145 );
        Element e74= new Element (R.string.tungsten,R.string.tungsten_hintu, R.string.tungsten_info, R.string.tungsten_name, R.string.tmetals, box74, textviewTungsten, R.string.tungsten_el_config, 183.84f, 7.864f, 2.36f, 3695.0f, 5828.0f, 135 );
        Element e75= new Element (R.string.rhenium,R.string.rhenium_hintu, R.string.rhenium_info, R.string.rhenium_name, R.string.tmetals, box75, textviewRhenium, R.string.rhenium_el_config, 186.207f, 7.834f, 1.90f, 3459.0f, 5869.0f, 135 );
        Element e76= new Element (R.string.osmium,R.string.osmium_hintu, R.string.osmium_info, R.string.osmium_name, R.string.tmetals, box76, textviewOsmium, R.string.osmium_el_config, 190.23f, 8.438f, 2.20f, 3306.0f, 5285.0f, 130 );
        Element e77= new Element (R.string.iridium,R.string.iridium_hintu, R.string.iridium_info, R.string.iridium_name, R.string.tmetals, box77, textviewIridium, R.string.iridium_el_config, 192.217f, 8.967f, 2.20f, 2719.0f, 4701.0f, 135 );
        Element e78= new Element (R.string.platinum,R.string.platinum_hintu, R.string.platinum_info, R.string.platinum_name, R.string.tmetals, box78, textviewPlatinum, R.string.platinum_el_config, 195.084f, 8.959f, 2.28f, 2041.4f, 4098.0f, 135 );
        Element e79= new Element (R.string.gold,R.string.gold_hintu, R.string.gold_info, R.string.gold_name, R.string.tmetals, box79, textviewGold, R.string.gold_el_config, 196.967f, 9.226f, 2.54f, 1337.33f, 3129.0f, 135 );
        Element e80= new Element (R.string.mercury,R.string.mercury_hintu, R.string.mercury_info, R.string.mercury_name, R.string.tmetals, box80, textviewMercury, R.string.mercury_el_config, 200.592f, 10.438f, 2.00f, 234.43f, 629.88f, 150 );

        Element e81= new Element (R.string.thallium,R.string.thallium_hintu, R.string.thallium_info, R.string.thallium_name, R.string.posttm, box81, textviewThallium, R.string.thallium_el_config, 204.38f, 6.108f, 1.62f, 577.0f, 1746.0f, 190 );
        Element e82= new Element (R.string.lead,R.string.lead_hintu, R.string.lead_info, R.string.lead_name, R.string.posttm, box82, textviewLead, R.string.lead_el_config, 207.2f, 7.417f, 1.87f, 600.61f, 2022.0f, 180 );
        Element e83= new Element (R.string.bismuth,R.string.bismuth_hintu, R.string.bismuth_info, R.string.bismuth_name, R.string.posttm, box83, textviewBismuth, R.string.bismuth_el_config, 208.98f, 7.286f, 2.02f, 544.7f, 1837.0f, 160 );
        Element e84= new Element (R.string.polonium,R.string.polonium_hintu, R.string.polonium_info, R.string.polonium_name, R.string.metalloids, box84, textviewPolonium, R.string.polonium_el_config, 209.0f, 8.414f, 2.00f, 527.0f, 1235.0f, 190 );
        Element e85= new Element (R.string.astatine,R.string.astatine_hintu, R.string.astatine_info, R.string.astatine_name, R.string.halogens, box85, textviewAstatine, R.string.astatine_el_config, 210.0f, 9.318f, 2.20f, 575.0f, 610.0f, 0 );
        Element e86= new Element (R.string.radon,R.string.radon_hintu, R.string.radon_info, R.string.radon_name, R.string.ngases, box86, textviewRadon, R.string.radon_el_config, 222.0f, 10.749f, 2.20f, 202.0f, 211.3f, 0 );
        Element e87= new Element (R.string.francium, R.string.francium_hintu, R.string.francium_info,R.string.francium_name, R.string.alkaline, box87, textviewFrancium, R.string.francium_el_config, 223.0f, 4.073f, 0.70f, 300.0f, 950.0f, 0);
        Element e88= new Element (R.string.radium,R.string.radium_hintu, R.string.radium_info, R.string.radium_name, R.string.alk_earth, box88, textviewRadium, R.string.radium_el_config, 226.0f, 5.278f, 0.90f, 973.0f, 2010.0f, 215 );
        Element e89= new Element (R.string.actinium,R.string.actinium_hintu, R.string.actinium_info, R.string.actinium_name, R.string.actinides, box89, textviewActinium, R.string.actinium_el_config, 277.0f, 5.38f, 1.10f, 1323.0f, 3471.0f, 195 );
        Element e90= new Element (R.string.thorium,R.string.thorium_hintu, R.string.thorium_info, R.string.thorium_name, R.string.actinides, box90, textviewThorium, R.string.thorium_el_config, 232.038f, 6.307f, 1.30f, 2115.0f, 5061.0f, 180 );

        Element e91= new Element (R.string.protactinium,R.string.protactinium_hintu, R.string.protactinium_info, R.string.protactinium_name, R.string.actinides, box91, textviewProtactinium, R.string.protactinium_el_config, 231.036f, 5.89f, 1.50f, 1841.0f, 4300.0f, 180 );
        Element e92= new Element (R.string.uranium,R.string.uranium_hintu, R.string.uranium_info, R.string.uranium_name, R.string.actinides, box92, textviewUranium, R.string.uranium_el_config, 238.029f, 6.194f, 1.38f, 1405.3f, 4404.0f, 175 );
        Element e93= new Element (R.string.neptunium,R.string.neptunium_hintu, R.string.neptunium_info, R.string.neptunium_name, R.string.actinides, box93, textviewNeptunium, R.string.neptunium_el_config, 237.0f, 6.266f, 1.36f, 917.0f, 4273.0f, 175 );
        Element e94= new Element (R.string.plutonium,R.string.plutonium_hintu, R.string.plutonium_info, R.string.plutonium_name, R.string.actinides, box94, textviewPlutonium, R.string.plutonium_el_config, 244.0f, 6.026f, 1.28f, 912.5f, 3501.0f, 175 );
        Element e95= new Element (R.string.americium,R.string.americium_hintu, R.string.americium_info, R.string.americium_name, R.string.actinides, box95, textviewAmericium, R.string.americium_el_config, 243.0f, 5.974f, 1.13f, 1449.0f, 2880.0f, 175 );
        Element e96= new Element (R.string.curium,R.string.curium_hintu, R.string.curium_info, R.string.curium_name, R.string.actinides, box96, textviewCurium, R.string.curium_el_config, 247.0f, 5.991f, 1.28f, 1613.0f, 3383.0f, 0 );
        Element e97= new Element (R.string.berkelium,R.string.berkelium_hintu, R.string.berkelium_info, R.string.berkelium_name, R.string.actinides, box97, textviewBerkelium, R.string.berkelium_el_config, 247.0f, 6.198f, 1.30f, 1259.0f, 2900.0f, 0 );
        Element e98= new Element (R.string.californium,R.string.californium_hintu, R.string.californium_info, R.string.californium_name, R.string.actinides, box98, textviewCalifornium, R.string.californium_el_config, 251.0f, 6.282f, 1.30f, 1173.0f, 1743.0f, 0 );
        Element e99= new Element (R.string.einsteinium,R.string.einsteinium_hintu, R.string.einsteinium_info, R.string.einsteinium_name, R.string.actinides, box99, textviewEinsteinium, R.string.einsteinium_el_config, 252.0f, 6.368f, 1.30f, 1133.0f, 1269.0f, 0 );
        Element e100= new Element (R.string.fermium,R.string.fermium_hintu, R.string.fermium_info, R.string.fermium_name, R.string.actinides, box100, textviewFermium, R.string.fermium_el_config, 257.0f, 6.50f, 1.30f, 1125.0f, 0, 0 );

        Element e101= new Element (R.string.mendelevium,R.string.mendelevium_hintu, R.string.mendelevium_info, R.string.mendelevium_name, R.string.actinides, box101, textviewMendelevium, R.string.mendelevium_el_config, 258.0f, 6.58f, 1.30f, 1100.0f, 0, 0 );
        Element e102= new Element (R.string.nobelium,R.string.nobelium_hintu, R.string.nobelium_info, R.string.nobelium_name, R.string.actinides, box102, textviewNobelium, R.string.nobelium_el_config, 259.0f, 6.66f, 1.30f, 1100.0f, 0, 0 );
        Element e103= new Element (R.string.lawrencium,R.string.lawrencium_hintu, R.string.lawrencium_info, R.string.lawrencium_name, R.string.actinides, box103, textviewLawrencium, R.string.lawrencium_el_config, 266.0f, 4.96f, 1.30f, 1900.0f, 0, 0 );
        Element e104= new Element (R.string.rutherfordium, R.string.rutherfordium_hintu, R.string.rutherfordium_info, R.string.rutherfordium_name, R.string.tmetals, box104, textviewRutherfordium, R.string.rutherfordium_el_config, 267.0f, 6.02f, 0, 2400.0f, 5800.0f, 0 );
        Element e105= new Element (R.string.dubnium,R.string.dubnium_hintu, R.string.dubnium_info, R.string.dubnium_name, R.string.tmetals, box105, textviewDubnium, R.string.dubnium_el_config, 268.0f, 6.8f, 0, 0, 0, 0 );
        Element e106= new Element (R.string.seaborgium,R.string.seaborgium_hintu, R.string.seaborgium_info, R.string.seaborgium_name, R.string.tmetals, box106, textviewSeaborgium, R.string.seaborgium_el_config, 271.0f, 7.8f, 0, 0, 0, 0 );
        Element e107= new Element (R.string.bohrium,R.string.bohrium_hintu, R.string.bohrium_info, R.string.bohrium_name, R.string.tmetals, box107, textviewBohrium, R.string.bohrium_el_config, 270.0f, 7.7f, 0, 0, 0, 0 );
        Element e108= new Element (R.string.hassium,R.string.hassium_hintu, R.string.hassium_info, R.string.hassium_name, R.string.tmetals, box108, textviewHassium, R.string.hassium_el_config, 269.0f, 7.6f, 0, 0, 0, 0 );
        Element e109= new Element (R.string.meitnerium,R.string.meitnerium_hintu, R.string.meitnerium_info, R.string.meitnerium_name, R.string.unknown_type, box109, textviewMeitnerium, R.string.meitnerium_el_config, 278.0f, 0.0f, 0, 0, 0, 0 );
        Element e110= new Element (R.string.darmstadtium,R.string.darmstadtium_hintu, R.string.darmstadtium_info, R.string.darmstadtium_name, R.string.unknown_type, box110, textviewDarmstadtium, R.string.darmstadtium_el_config, 281.0f, 0.0f, 0, 0, 0, 0 );

        Element e111= new Element (R.string.roentgenium, R.string.roentgenium_hintu, R.string.roentgenium_info, R.string.roentgenium_name, R.string.unknown_type, box111, textviewRoentgenium, R.string.roentgenium_el_config, 282.0f, 0.0f, 0, 0, 0, 0  );
        Element e112= new Element (R.string.copernicium,R.string.copernicium_hintu, R.string.copernicium_info, R.string.copernicium_name, R.string.unknown_type, box112, textviewCopernicium, R.string.copernicium_el_config, 285.0f, 0.0f, 0, 0, 0, 0 );
        Element e113= new Element (R.string.nihonium,R.string.nihonium_hintu, R.string.nihonium_info, R.string.nihonium_name, R.string.unknown_type, box113, textviewNihonium, R.string.nihonium_el_config, 286.0f, 0.0f, 0, 0, 0, 0 );
        Element e114= new Element (R.string.flerovium,R.string.flerovium_hintu, R.string.flerovium_info, R.string.flerovium_name, R.string.unknown_type, box114, textviewFlerovium, R.string.flerovium_el_config, 289.0f, 0.0f, 0, 0, 0, 0 );
        Element e115= new Element (R.string.moscovium,R.string.moscovium_hintu, R.string.moscovium_info, R.string.moscovium_name, R.string.unknown_type, box115, textviewMoscovium, R.string.moscovium_el_config, 289.0f, 0.0f, 0, 0, 0, 0 );
        Element e116= new Element (R.string.livermorium,R.string.livermorium_hintu, R.string.livermorium_info, R.string.livermorium_name, R.string.unknown_type, box116, textviewLivermorium, R.string.livermorium_el_config, 293.0f, 0.0f, 0, 0, 0, 0 );
        Element e117= new Element (R.string.tennessine,R.string.tennessine_hintu, R.string.tennessine_info, R.string.tennessine_name, R.string.unknown_type, box117, textviewTennessine, R.string.tennessine_el_config, 294.0f, 0.0f, 0, 0, 0, 0 );
        Element e118= new Element (R.string.oganesson,R.string.oganesson_hintu, R.string.oganesson_info, R.string.oganesson_name, R.string.unknown_type, box118, textviewOganesson, R.string.oganesson_el_config, 294.0f, 0.0f, 0, 0, 0, 0 );


        if (level_type.equals( "Group" )|| level_type.equals( "Element_list" )) {
            add_element_for_level(e1 );
            add_element_for_level(e2 );
            add_element_for_level(e3 );
            add_element_for_level(e4 );
            add_element_for_level(e5 );
            add_element_for_level(e6 );
            add_element_for_level(e7 );
            add_element_for_level(e8 );
            add_element_for_level(e9 );
            add_element_for_level(e10 );
            add_element_for_level(e11 );
            add_element_for_level(e12 );
            add_element_for_level(e13 );
            add_element_for_level(e14 );
            add_element_for_level(e15 );
            add_element_for_level(e16 );
            add_element_for_level(e17 );
            add_element_for_level(e18 );
            add_element_for_level(e19 );
            add_element_for_level(e20 );
            add_element_for_level(e21 );
            add_element_for_level(e22 );
            add_element_for_level(e23 );
            add_element_for_level(e24 );
            add_element_for_level(e25 );
            add_element_for_level(e26 );
            add_element_for_level(e27 );
            add_element_for_level(e28 );
            add_element_for_level(e29 );
            add_element_for_level(e30 );
            add_element_for_level(e31 );
            add_element_for_level(e32 );
            add_element_for_level(e33 );
            add_element_for_level(e34 );
            add_element_for_level(e35 );
            add_element_for_level(e36 );
            add_element_for_level(e37 );
            add_element_for_level(e38 );
            add_element_for_level(e39 );
            add_element_for_level(e40 );
            add_element_for_level(e41 );
            add_element_for_level(e42 );
            add_element_for_level(e43 );
            add_element_for_level(e44 );
            add_element_for_level(e45 );
            add_element_for_level(e46 );
            add_element_for_level(e47 );
            add_element_for_level(e48 );
            add_element_for_level(e49 );
            add_element_for_level(e50 );
            add_element_for_level(e51);
            add_element_for_level(e52 );
            add_element_for_level(e53 );
            add_element_for_level(e54 );
            add_element_for_level(e55 );
            add_element_for_level(e56 );
            add_element_for_level(e57 );
            add_element_for_level(e58 );
            add_element_for_level(e59 );
            add_element_for_level(e60 );
            add_element_for_level(e61 );
            add_element_for_level(e62 );
            add_element_for_level(e63 );
            add_element_for_level(e64 );
            add_element_for_level(e65 );
            add_element_for_level(e66 );
            add_element_for_level(e67 );
            add_element_for_level(e68 );
            add_element_for_level(e69 );
            add_element_for_level(e70 );
            add_element_for_level(e71 );
            add_element_for_level(e72 );
            add_element_for_level(e73 );
            add_element_for_level(e74 );
            add_element_for_level(e75 );
            add_element_for_level(e76 );
            add_element_for_level(e77 );
            add_element_for_level(e78 );
            add_element_for_level(e79 );
            add_element_for_level(e80 );
            add_element_for_level(e81 );
            add_element_for_level(e82 );
            add_element_for_level(e83 );
            add_element_for_level(e84 );
            add_element_for_level(e85 );
            add_element_for_level(e86 );
            add_element_for_level(e87 );
            add_element_for_level(e88 );
            add_element_for_level(e89 );
            add_element_for_level(e90 );
            add_element_for_level(e91 );
            add_element_for_level(e92 );
            add_element_for_level(e93 );
            add_element_for_level(e94 );
            add_element_for_level(e95 );
            add_element_for_level(e96 );
            add_element_for_level(e97 );
            add_element_for_level(e98 );
            add_element_for_level(e99 );
            add_element_for_level(e100 );
            add_element_for_level(e101 );
            add_element_for_level(e102 );
            add_element_for_level(e103 );
            add_element_for_level(e104 );
            add_element_for_level(e105 );
            add_element_for_level(e106 );
            add_element_for_level(e107 );
            add_element_for_level(e108 );
            add_element_for_level(e109 );
            add_element_for_level(e110 );
            add_element_for_level(e111 );
            add_element_for_level(e112 );
            add_element_for_level(e113 );
            add_element_for_level(e114 );
            add_element_for_level(e115 );
            add_element_for_level(e116 );
            add_element_for_level(e117 );
            add_element_for_level(e118 );
        }

        else if (level_type.equals( "Random" )|| level_type.equals( "Whole" )) {

            l.add( e1 );
            l.add( e2 );
            l.add( e3 );
            l.add( e4 );
            l.add( e5 );
            l.add( e6 );
            l.add( e7 );
            l.add( e8 );
            l.add( e9 );
            l.add( e10 );
            l.add( e11 );
            l.add( e12 );
            l.add( e13 );
            l.add( e14 );
            l.add( e15 );
            l.add( e16 );
            l.add( e17 );
            l.add( e18 );
            l.add( e19 );
            l.add( e20 );
            l.add( e21 );
            l.add( e22);
            l.add( e23 );
            l.add( e24 );
            l.add( e25 );
            l.add( e26 );
            l.add( e27 );
            l.add( e28 );
            l.add( e29 );
            l.add( e30 );
            l.add( e31 );
            l.add( e32 );
            l.add( e33 );
            l.add( e34 );
            l.add( e35 );
            l.add( e36 );
            l.add( e37 );
            l.add( e38 );
            l.add( e39 );
            l.add( e40 );
            l.add( e41 );
            l.add( e42 );
            l.add( e43 );
            l.add( e44 );
            l.add( e45 );
            l.add( e46 );
            l.add( e47 );
            l.add( e48 );
            l.add( e49 );
            l.add( e50 );
            l.add( e51 );
            l.add( e52 );
            l.add( e53 );
            l.add( e54 );
            l.add( e55 );
            l.add( e56 );
            l.add( e57 );
            l.add( e58 );
            l.add( e59 );
            l.add( e60 );
            l.add( e61 );
            l.add( e62 );
            l.add( e63 );
            l.add( e64 );
            l.add( e65 );
            l.add( e66 );
            l.add( e67 );
            l.add( e68 );
            l.add( e69 );
            l.add( e70 );
            l.add( e71 );
            l.add( e72 );
            l.add( e73 );
            l.add( e74 );
            l.add( e75 );
            l.add( e76 );
            l.add( e77 );
            l.add( e78 );
            l.add( e79 );
            l.add( e80 );
            l.add( e81 );
            l.add( e82 );
            l.add( e83 );
            l.add( e84 );
            l.add( e85 );
            l.add( e86 );
            l.add( e87 );
            l.add( e88 );
            l.add( e89 );
            l.add( e90 );
            l.add( e91 );
            l.add( e92 );
            l.add( e93 );
            l.add( e94 );
            l.add( e95 );
            l.add( e96 );
            l.add( e97 );
            l.add( e98 );
            l.add( e99 );
            l.add( e100 );
            l.add( e101 );
            l.add( e102 );
            l.add( e103 );
            l.add( e104 );
            l.add( e105 );
            l.add( e106 );
            l.add( e107 );
            l.add( e108 );
            l.add( e109 );
            l.add( e110 );
            l.add( e111 );
            l.add( e112 );
            l.add( e113 );
            l.add( e114 );
            l.add( e115 );
            l.add( e116 );
            l.add( e117);
            l.add( e118 );

            for (int j =0; j < l.size(); j++){
                Element m= l.get(j);
                m.getTextView().setVisibility( INVISIBLE );
            }

            if (level_type.equals( "Random" )){
                add_random_for_level( l );}
        }
        else { }






        //Make a random box float
        floating_box();

        //Generate a random element in the random box
        Element rand_element = l.get( new Random().nextInt(l.size()));
        textviewRandom.setText(rand_element.getSymbol());
        hintDisplayText.setText(rand_element.getHint());
        randomInfoDisplayText.setText(rand_element.getRandomInfo());

        //Hint button
        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hintDisplay.getVisibility()== INVISIBLE){
                    hintDisplay.setVisibility( VISIBLE );
                    hintDisplay.bringToFront();
                    read_animation(); }
                else if (hintDisplay.getVisibility()==View.VISIBLE){
                    cancel_animation_and_hint(); }
                else
                    hintDisplay.setVisibility( INVISIBLE ); }});

        hintDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel_animation_and_hint(); }});

        //Reset button
        textviewReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v==textviewReset) {
                    recreate(); } } });

        //Place button
        placeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel_animation_and_hint();
                if (t != null) {
                    t.cancel();
                    startTimer( 20000, 1000 );
                    timeout=1;
                    if (box0.getVisibility()== VISIBLE){
                        error_count++;
                        error.setText(""+ (error_count) );
                    } }

                if (textviewRandom.getText().toString().equals("H")){
                    textviewHydrogen.setVisibility( VISIBLE);
                    place_animation (R.string.hydrogen);
                    remove_el_from_db( elem, l, R.string.hydrogen ); }

                else if (textviewRandom.getText().toString().equals("He")){
                    textviewHelium.setVisibility( VISIBLE);
                    place_animation (R.string.helium);
                    remove_el_from_db( elem, l, R.string.helium ); }

                else if (textviewRandom.getText().toString().equals("Li")){
                    textviewLithium.setVisibility( VISIBLE);
                    place_animation (R.string.lithium);
                    remove_el_from_db( elem, l, R.string.lithium ); }

                else if (textviewRandom.getText().equals("Be")){
                    textviewBerillium.setVisibility( VISIBLE);
                    place_animation (R.string.berillium);
                    remove_el_from_db( elem, l, R.string.berillium ); }

                else if (textviewRandom.getText().toString().equals("B")){
                    textviewBoron.setVisibility( VISIBLE);
                    place_animation (R.string.boron);
                    remove_el_from_db( elem, l, R.string.boron ); }

                else if (textviewRandom.getText().toString().equals("C")){
                    textviewCarbon.setVisibility( VISIBLE);
                    place_animation (R.string.carbon);
                    remove_el_from_db( elem, l, R.string.carbon ); }

                else if (textviewRandom.getText().toString().equals("N")){
                    textviewNitrogen.setVisibility( VISIBLE);
                    place_animation (R.string.nitrogen);
                    remove_el_from_db( elem, l, R.string.nitrogen ); }

                else if (textviewRandom.getText().toString().equals("O")){
                    textviewOxygen.setVisibility( VISIBLE);
                    place_animation (R.string.oxygen);
                    remove_el_from_db( elem, l, R.string.oxygen ); }

                else if (textviewRandom.getText().toString().equals("F")){
                    textviewFluorine.setVisibility( VISIBLE);
                    place_animation (R.string.fluorine);
                    remove_el_from_db( elem, l, R.string.fluorine ); }

                else if (textviewRandom.getText().toString().equals("Ne")){
                    textviewNeon.setVisibility( VISIBLE);
                    place_animation (R.string.neon);
                    remove_el_from_db( elem, l, R.string.neon ); }

                else if (textviewRandom.getText().equals("Na")){
                    textviewSodium.setVisibility( VISIBLE);
                    place_animation (R.string.sodium);
                    remove_el_from_db( elem, l, R.string.sodium); }

                else if (textviewRandom.getText().toString().equals("Mg")){
                    textviewMagnesium.setVisibility( VISIBLE);
                    place_animation (R.string.magnesium);
                    remove_el_from_db( elem, l, R.string.magnesium ); }

                else if (textviewRandom.getText().equals("Al")){
                    textviewAluminium.setVisibility( VISIBLE);
                    place_animation (R.string.aluminium);
                    remove_el_from_db( elem, l, R.string.aluminium); }

                else if (textviewRandom.getText().equals("Si")){
                    textviewSilicon.setVisibility( VISIBLE);
                    place_animation (R.string.silicon);
                    remove_el_from_db( elem, l, R.string.silicon); }

                else if (textviewRandom.getText().equals("P")){
                    textviewPhosphorous.setVisibility( VISIBLE);
                    place_animation (R.string.phosphorous);
                    remove_el_from_db( elem, l, R.string.phosphorous); }

                else if (textviewRandom.getText().equals("S")){
                    textviewSulfur.setVisibility( VISIBLE);
                    place_animation (R.string.sulfur);
                    remove_el_from_db( elem, l, R.string.sulfur); }

                else if (textviewRandom.getText().equals("Cl")){
                    textviewChlorine.setVisibility( VISIBLE);
                    place_animation (R.string.chlorine);
                    remove_el_from_db( elem, l, R.string.chlorine); }

                else if (textviewRandom.getText().equals("Ar")){
                    textviewArgon.setVisibility( VISIBLE);
                    place_animation (R.string.argon);
                    remove_el_from_db( elem, l, R.string.argon); }

                else if (textviewRandom.getText().equals("K")){
                    textviewPotassium.setVisibility( VISIBLE);
                    place_animation (R.string.potassium);
                    remove_el_from_db( elem, l, R.string.potassium ); }

                else if (textviewRandom.getText().equals("Ca")){
                    textviewCalcium.setVisibility( VISIBLE);
                    place_animation (R.string.calcium);
                    remove_el_from_db( elem, l, R.string.calcium); }

                else if (textviewRandom.getText().equals("Sc")){
                    textviewScandium.setVisibility( VISIBLE);
                    place_animation (R.string.scandium);
                    remove_el_from_db( elem, l, R.string.scandium); }

                else if (textviewRandom.getText().equals("Ti")){
                    textviewTitanium.setVisibility( VISIBLE);
                    place_animation (R.string.titanium);
                    remove_el_from_db( elem, l, R.string.titanium); }

                else if (textviewRandom.getText().equals("V")){
                    textviewVanadium.setVisibility( VISIBLE);
                    place_animation (R.string.vanadium);
                    remove_el_from_db( elem, l, R.string.vanadium); }

                else if (textviewRandom.getText().equals("Cr")){
                    textviewChromium.setVisibility( VISIBLE);
                    place_animation (R.string.chromium);
                    remove_el_from_db( elem, l, R.string.chromium); }

                else if (textviewRandom.getText().equals("Mn")){
                    textviewManganese.setVisibility( VISIBLE);
                    place_animation (R.string.manganese);
                    remove_el_from_db( elem, l, R.string.manganese ); }

                else if (textviewRandom.getText().equals("Fe")){
                    textviewIron.setVisibility( VISIBLE);
                    place_animation (R.string.iron);
                    remove_el_from_db( elem, l, R.string.iron ); }

                else if (textviewRandom.getText().equals("Co")){
                    textviewCobalt.setVisibility( VISIBLE);
                    place_animation (R.string.cobalt);
                    remove_el_from_db( elem, l, R.string.cobalt); }

                else if (textviewRandom.getText().equals("Ni")){
                    textviewNickel.setVisibility( VISIBLE);
                    place_animation (R.string.nickel);
                    remove_el_from_db( elem, l, R.string.nickel); }

                else if (textviewRandom.getText().equals("Cu")){
                    textviewCopper.setVisibility( VISIBLE);
                    place_animation (R.string.copper);
                    remove_el_from_db( elem, l, R.string.copper); }

                else if (textviewRandom.getText().equals("Zn")){
                    textviewZinc.setVisibility( VISIBLE);
                    place_animation (R.string.zinc);
                    remove_el_from_db( elem, l, R.string.zinc); }

                else if (textviewRandom.getText().equals("Ga")){
                    textviewGallium.setVisibility( VISIBLE);
                    place_animation (R.string.gallium);
                    remove_el_from_db( elem, l, R.string.gallium); }

                else if (textviewRandom.getText().equals("Ge")){
                    textviewGermanium.setVisibility( VISIBLE);
                    place_animation (R.string.germanium);
                    remove_el_from_db( elem, l, R.string.germanium); }

                else if (textviewRandom.getText().equals("As")){
                    textviewArsenic.setVisibility( VISIBLE);
                    place_animation (R.string.arsenic);
                    remove_el_from_db( elem, l, R.string.arsenic); }

                else if (textviewRandom.getText().equals("Se")){
                    textviewSelenium.setVisibility( VISIBLE);
                    place_animation (R.string.selenium);
                    remove_el_from_db( elem, l, R.string.selenium); }

                else if (textviewRandom.getText().equals("Br")){
                    textviewBromine.setVisibility( VISIBLE);
                    place_animation (R.string.bromine);
                    remove_el_from_db( elem, l, R.string.bromine); }

                else if (textviewRandom.getText().equals("Kr")){
                    textviewKrypton.setVisibility( VISIBLE);
                    place_animation (R.string.krypton);
                    remove_el_from_db( elem, l, R.string.krypton); }

                else if (textviewRandom.getText().equals("Rb")){
                    textviewRubidium.setVisibility( VISIBLE);
                    place_animation (R.string.rubidium);
                    remove_el_from_db( elem, l, R.string.rubidium); }

                else if (textviewRandom.getText().equals("Sr")){
                    textviewStroncium.setVisibility( VISIBLE);
                    place_animation (R.string.strontium);
                    remove_el_from_db( elem, l, R.string.strontium ); }

                else if (textviewRandom.getText().equals("Y")){
                    textviewYttrium.setVisibility( VISIBLE);
                    place_animation (R.string.yttrium);
                    remove_el_from_db( elem, l, R.string.yttrium); }

                else if (textviewRandom.getText().equals("Zr")){
                    textviewZirconium.setVisibility( VISIBLE);
                    place_animation (R.string.zirconium);
                    remove_el_from_db( elem, l, R.string.zirconium); }

                else if (textviewRandom.getText().equals("Nb")){
                    textviewNiobium.setVisibility( VISIBLE);
                    place_animation (R.string.niobium);
                    remove_el_from_db( elem, l, R.string.niobium); }

                else if (textviewRandom.getText().equals("Mo")){
                    textviewMolybdenum.setVisibility( VISIBLE);
                    place_animation (R.string.molybdenum);
                    remove_el_from_db( elem, l, R.string.molybdenum); }

                else if (textviewRandom.getText().equals("Tc")){
                    textviewTechnetium.setVisibility( VISIBLE);
                    place_animation (R.string.technetium);
                    remove_el_from_db( elem, l, R.string.technetium); }

                else if (textviewRandom.getText().equals("Ru")){
                    textviewRuthenium.setVisibility( VISIBLE);
                    place_animation (R.string.ruthenium);
                    remove_el_from_db( elem, l, R.string.ruthenium); }

                else if (textviewRandom.getText().equals("Rh")){
                    textviewRhodium.setVisibility( VISIBLE);
                    place_animation (R.string.rhodium);
                    remove_el_from_db( elem, l, R.string.rhodium); }

                else if (textviewRandom.getText().equals("Pd")){
                    textviewPalladium.setVisibility( VISIBLE);
                    place_animation (R.string.palladium);
                    remove_el_from_db( elem, l, R.string.palladium); }

                else if (textviewRandom.getText().equals("Ag")){
                    textviewSilver.setVisibility( VISIBLE);
                    place_animation (R.string.silver);
                    remove_el_from_db( elem, l, R.string.silver); }

                else if (textviewRandom.getText().equals("Cd")){
                    textviewCadmium.setVisibility( VISIBLE);
                    place_animation (R.string.cadmium);
                    remove_el_from_db( elem, l, R.string.cadmium); }

                else if (textviewRandom.getText().equals("In")){
                    textviewIndium.setVisibility( VISIBLE);
                    place_animation (R.string.indium);
                    remove_el_from_db( elem, l, R.string.indium); }

                else if (textviewRandom.getText().equals("Sn")){
                    textviewTin.setVisibility( VISIBLE);
                    place_animation (R.string.tin);
                    remove_el_from_db( elem, l, R.string.tin); }

                else if (textviewRandom.getText().equals("Sb")){
                    textviewAntimony.setVisibility( VISIBLE);
                    place_animation (R.string.antimony);
                    remove_el_from_db( elem, l, R.string.antimony); }

                else if (textviewRandom.getText().equals("Te")){
                    textviewTellurium.setVisibility( VISIBLE);
                    place_animation (R.string.tellurium);
                    remove_el_from_db( elem, l, R.string.tellurium); }

                else if (textviewRandom.getText().equals("I")){
                    textviewIodine.setVisibility( VISIBLE);
                    place_animation (R.string.iodine);
                    remove_el_from_db( elem, l, R.string.iodine); }

                else if (textviewRandom.getText().equals("Xe")){
                    textviewXenon.setVisibility( VISIBLE);
                    place_animation (R.string.xenon);
                    remove_el_from_db( elem, l, R.string.xenon); }

                else if (textviewRandom.getText().equals("Cs")){
                    textviewCaesium.setVisibility( VISIBLE);
                    place_animation (R.string.caesium);
                    remove_el_from_db( elem, l, R.string.caesium); }

                else if (textviewRandom.getText().equals("Ba")){
                    textviewBarium.setVisibility( VISIBLE);
                    place_animation (R.string.barium);
                    remove_el_from_db( elem, l, R.string.barium); }

                else if (textviewRandom.getText().equals("Hf")){
                    textviewHafnium.setVisibility( VISIBLE);
                    place_animation (R.string.hafnium);
                    remove_el_from_db( elem, l, R.string.hafnium); }

                else if (textviewRandom.getText().equals("Ta")){
                    textviewTantalum.setVisibility( VISIBLE);
                    place_animation (R.string.tantalum);
                    remove_el_from_db( elem, l, R.string.tantalum); }

                else if (textviewRandom.getText().equals("W")){
                    textviewTungsten.setVisibility( VISIBLE);
                    place_animation (R.string.tungsten);
                    remove_el_from_db( elem, l, R.string.tungsten); }

                else if (textviewRandom.getText().equals("Re")){
                    textviewRhenium.setVisibility( VISIBLE);
                    place_animation (R.string.rhenium);
                    remove_el_from_db( elem, l, R.string.rhenium); }

                else if (textviewRandom.getText().equals("Os")){
                    textviewOsmium.setVisibility( VISIBLE);
                    place_animation (R.string.osmium);
                    remove_el_from_db( elem, l, R.string.osmium); }

                else if (textviewRandom.getText().equals("Ir")){
                    textviewIridium.setVisibility( VISIBLE);
                    place_animation (R.string.iridium);
                    remove_el_from_db( elem, l, R.string.iridium); }

                else if (textviewRandom.getText().equals("Pt")){
                    textviewPlatinum.setVisibility( VISIBLE);
                    place_animation (R.string.platinum);
                    remove_el_from_db( elem, l, R.string.platinum); }

                else if (textviewRandom.getText().equals("Au")){
                    textviewGold.setVisibility( VISIBLE);
                    place_animation (R.string.gold);
                    remove_el_from_db( elem, l, R.string.gold); }

                else if (textviewRandom.getText().equals("Hg")){
                    textviewMercury.setVisibility( VISIBLE);
                    place_animation (R.string.mercury);
                    remove_el_from_db( elem, l, R.string.mercury); }

                else if (textviewRandom.getText().equals("Tl")){
                    textviewThallium.setVisibility( VISIBLE);
                    place_animation (R.string.thallium);
                    remove_el_from_db( elem, l, R.string.thallium); }

                else if (textviewRandom.getText().equals("Pb")){
                    textviewLead.setVisibility( VISIBLE);
                    place_animation (R.string.lead);
                    remove_el_from_db( elem, l, R.string.lead); }

                else if (textviewRandom.getText().equals("Bi")){
                    textviewBismuth.setVisibility( VISIBLE);
                    place_animation (R.string.bismuth);
                    remove_el_from_db( elem, l, R.string.bismuth); }

                else if (textviewRandom.getText().equals("Po")){
                    textviewPolonium.setVisibility( VISIBLE);
                    place_animation (R.string.polonium);
                    remove_el_from_db( elem, l, R.string.polonium); }

                else if (textviewRandom.getText().equals("At")){
                    textviewAstatine.setVisibility( VISIBLE);
                    place_animation (R.string.astatine);
                    remove_el_from_db( elem, l, R.string.astatine); }

                else if (textviewRandom.getText().equals("Rn")){
                    textviewRadon.setVisibility( VISIBLE);
                    place_animation (R.string.radon);
                    remove_el_from_db( elem, l, R.string.radon); }

                else if (textviewRandom.getText().equals("Fr")){
                    textviewFrancium.setVisibility( VISIBLE);
                    place_animation (R.string.francium);
                    remove_el_from_db( elem, l, R.string.francium); }

                else if (textviewRandom.getText().equals("Ra")){
                    textviewRadium.setVisibility( VISIBLE);
                    place_animation (R.string.radium);
                    remove_el_from_db( elem, l, R.string.radium); }

                else if (textviewRandom.getText().equals("Rf")){
                    textviewRutherfordium.setVisibility( VISIBLE);
                    place_animation (R.string.rutherfordium);
                    remove_el_from_db( elem, l, R.string.rutherfordium); }

                else if (textviewRandom.getText().equals("Db")){
                    textviewDubnium.setVisibility( VISIBLE);
                    place_animation (R.string.dubnium);
                    remove_el_from_db( elem, l, R.string.dubnium); }

                else if (textviewRandom.getText().equals("Sg")){
                    textviewSeaborgium.setVisibility( VISIBLE);
                    place_animation (R.string.seaborgium);
                    remove_el_from_db( elem, l, R.string.seaborgium); }

                else if (textviewRandom.getText().equals("Bh")){
                    textviewBohrium.setVisibility( VISIBLE);
                    place_animation (R.string.bohrium);
                    remove_el_from_db( elem, l, R.string.bohrium); }

                else if (textviewRandom.getText().equals("Hs")){
                    textviewHassium.setVisibility( VISIBLE);
                    place_animation (R.string.hassium);
                    remove_el_from_db( elem, l, R.string.hassium); }

                else if (textviewRandom.getText().equals("Mt")){
                    textviewMeitnerium.setVisibility( VISIBLE);
                    place_animation (R.string.meitnerium);
                    remove_el_from_db( elem, l, R.string.meitnerium); }

                else if (textviewRandom.getText().equals("Ds")){
                    textviewDarmstadtium.setVisibility( VISIBLE);
                    place_animation (R.string.darmstadtium);
                    remove_el_from_db( elem, l, R.string.darmstadtium); }

                else if (textviewRandom.getText().equals("Rg")){
                    textviewRoentgenium.setVisibility( VISIBLE);
                    place_animation (R.string.roentgenium);
                    remove_el_from_db( elem, l, R.string.roentgenium); }

                else if (textviewRandom.getText().equals("Cn")){
                    textviewCopernicium.setVisibility( VISIBLE);
                    place_animation (R.string.copernicium);
                    remove_el_from_db( elem, l, R.string.copernicium); }

                else if (textviewRandom.getText().equals("Nh")){
                    textviewNihonium.setVisibility( VISIBLE);
                    place_animation (R.string.nihonium);
                    remove_el_from_db( elem, l, R.string.nihonium); }

                else if (textviewRandom.getText().equals("Fl")){
                    textviewFlerovium.setVisibility( VISIBLE);
                    place_animation (R.string.flerovium);
                    remove_el_from_db( elem, l, R.string.flerovium); }

                else if (textviewRandom.getText().equals("Mc")){
                    textviewMoscovium.setVisibility( VISIBLE);
                    place_animation (R.string.moscovium);
                    remove_el_from_db( elem, l, R.string.moscovium); }

                else if (textviewRandom.getText().equals("Lv")){
                    textviewLivermorium.setVisibility( VISIBLE);
                    place_animation (R.string.livermorium);
                    remove_el_from_db( elem, l, R.string.livermorium); }

                else if (textviewRandom.getText().equals("Ts")){
                    textviewTennessine.setVisibility( VISIBLE);
                    place_animation (R.string.tennessine);
                    remove_el_from_db( elem, l, R.string.tennessine); }

                else if (textviewRandom.getText().equals("Og")){
                    textviewOganesson.setVisibility( VISIBLE);
                    place_animation (R.string.oganesson);
                    remove_el_from_db( elem, l, R.string.oganesson); }

                else if (textviewRandom.getText().equals("La")){
                    textviewLanthanum.setVisibility( VISIBLE);
                    place_animation (R.string.lanthanum);
                    remove_el_from_db( elem, l, R.string.lanthanum); }

                else if (textviewRandom.getText().equals("Ce")){
                    textviewCerium.setVisibility( VISIBLE);
                    place_animation (R.string.cerium);
                    remove_el_from_db( elem, l, R.string.cerium); }

                else if (textviewRandom.getText().equals("Pr")){
                    textviewPraseodymium.setVisibility( VISIBLE);
                    place_animation (R.string.praseodymium);
                    remove_el_from_db( elem, l, R.string.praseodymium); }

                else if (textviewRandom.getText().equals("Nd")){
                    textviewNeodymium.setVisibility( VISIBLE);
                    place_animation (R.string.neodymium);
                    remove_el_from_db( elem, l, R.string.neodymium); }

                else if (textviewRandom.getText().equals("Pm")){
                    textviewPromethium.setVisibility( VISIBLE);
                    place_animation (R.string.promethium);
                    remove_el_from_db( elem, l, R.string.promethium); }

                else if (textviewRandom.getText().equals("Sm")){
                    textviewSamarium.setVisibility( VISIBLE);
                    place_animation (R.string.samarium);
                    remove_el_from_db( elem, l, R.string.samarium); }

                else if (textviewRandom.getText().equals("Eu")){
                    textviewEuropium.setVisibility( VISIBLE);
                    place_animation (R.string.europium);
                    remove_el_from_db( elem, l, R.string.europium); }

                else if (textviewRandom.getText().equals("Gd")){
                    textviewGadolinium.setVisibility( VISIBLE);
                    place_animation (R.string.gadolinium);
                    remove_el_from_db( elem, l, R.string.gadolinium); }

                else if (textviewRandom.getText().equals("Tb")){
                    textviewTerbium.setVisibility( VISIBLE);
                    place_animation (R.string.terbium);
                    remove_el_from_db( elem, l, R.string.terbium); }

                else if (textviewRandom.getText().equals("Dy")){
                    textviewDysprosium.setVisibility( VISIBLE);
                    place_animation (R.string.dysprosium);
                    remove_el_from_db( elem, l, R.string.dysprosium); }

                else if (textviewRandom.getText().equals("Ho")){
                    textviewHolmium.setVisibility( VISIBLE);
                    place_animation (R.string.holmium);
                    remove_el_from_db( elem, l, R.string.holmium); }

                else if (textviewRandom.getText().equals("Er")){
                    textviewErbium.setVisibility( VISIBLE);
                    place_animation (R.string.erbium);
                    remove_el_from_db( elem, l, R.string.erbium); }

                else if (textviewRandom.getText().equals("Tm")){
                    textviewThulium.setVisibility( VISIBLE);
                    place_animation (R.string.thulium);
                    remove_el_from_db( elem, l, R.string.thulium); }

                else if (textviewRandom.getText().equals("Yb")){
                    textviewYtterbium.setVisibility( VISIBLE);
                    place_animation (R.string.ytterbium);
                    remove_el_from_db( elem, l, R.string.ytterbium); }

                else if (textviewRandom.getText().equals("Lu")){
                    textviewLutetium.setVisibility( VISIBLE);
                    place_animation (R.string.lutetium);
                    remove_el_from_db( elem, l, R.string.lutetium); }

                else if (textviewRandom.getText().equals("Ac")){
                    textviewActinium.setVisibility( VISIBLE);
                    place_animation (R.string.actinium);
                    remove_el_from_db( elem, l, R.string.actinium); }

                else if (textviewRandom.getText().equals("Th")){
                    textviewThorium.setVisibility( VISIBLE);
                    place_animation (R.string.thorium);
                    remove_el_from_db( elem, l, R.string.thorium); }

                else if (textviewRandom.getText().equals("Pa")){
                    textviewProtactinium.setVisibility( VISIBLE);
                    place_animation (R.string.protactinium);
                    remove_el_from_db( elem, l, R.string.protactinium); }

                else if (textviewRandom.getText().equals("U")){
                    textviewUranium.setVisibility( VISIBLE);
                    place_animation (R.string.uranium);
                    remove_el_from_db( elem, l, R.string.uranium); }

                else if (textviewRandom.getText().equals("Np")){
                    textviewNeptunium.setVisibility( VISIBLE);
                    place_animation (R.string.neptunium);
                    remove_el_from_db( elem, l, R.string.neptunium); }

                else if (textviewRandom.getText().equals("Pu")){
                    textviewPlutonium.setVisibility( VISIBLE);
                    place_animation (R.string.plutonium);
                    remove_el_from_db( elem, l, R.string.plutonium); }

                else if (textviewRandom.getText().equals("Am")){
                    textviewAmericium.setVisibility( VISIBLE);
                    place_animation (R.string.americium);
                    remove_el_from_db( elem, l, R.string.americium); }

                else if (textviewRandom.getText().equals("Cm")){
                    textviewCurium.setVisibility( VISIBLE);
                    place_animation (R.string.curium);
                    remove_el_from_db( elem, l, R.string.curium); }

                else if (textviewRandom.getText().equals("Bk")){
                    textviewBerkelium.setVisibility( VISIBLE);
                    place_animation (R.string.berkelium);
                    remove_el_from_db( elem, l, R.string.berkelium); }

                else if (textviewRandom.getText().equals("Cf")){
                    textviewCalifornium.setVisibility( VISIBLE);
                    place_animation (R.string.californium);
                    remove_el_from_db( elem, l, R.string.californium); }

                else if (textviewRandom.getText().equals("Es")){
                    textviewEinsteinium.setVisibility( VISIBLE);
                    place_animation (R.string.einsteinium);
                    remove_el_from_db( elem, l, R.string.einsteinium); }

                else if (textviewRandom.getText().equals("Fm")){
                    textviewFermium.setVisibility( VISIBLE);
                    place_animation (R.string.fermium);
                    remove_el_from_db( elem, l, R.string.fermium); }

                else if (textviewRandom.getText().equals("Md")){
                    textviewMendelevium.setVisibility( VISIBLE);
                    place_animation (R.string.mendelevium);
                    remove_el_from_db( elem, l, R.string.mendelevium); }

                else if (textviewRandom.getText().equals("No")){
                    textviewNobelium.setVisibility( VISIBLE);
                    place_animation (R.string.nobelium);
                    remove_el_from_db( elem, l, R.string.nobelium); }

                else if (textviewRandom.getText().equals("Lr")){
                    textviewLawrencium.setVisibility( VISIBLE);
                    place_animation (R.string.lawrencium);
                    remove_el_from_db( elem, l, R.string.lawrencium); }


            }});




        samelevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recreate();
            }});

        anotherlevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }});

    }




    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(view);
        ClipData data = ClipData.newPlainText("id", view.getResources().getResourceEntryName(view.getId()));
        view.startDrag(data, shadowBuilder, view, 0);
        cancel_animation_and_hint();
        return true; }


    @Override
    public boolean onDrag(View v, DragEvent dragEvent) {
        switch (dragEvent.getAction()) {

            // signal for the start of a drag and drop operation
            case DragEvent.ACTION_DRAG_STARTED:
                break;

            // the drag point has entered the bounding box of the View
            case DragEvent.ACTION_DRAG_ENTERED:
                v.setBackgroundResource( R.drawable.box1_d );
                break;

            // the user has moved the drag shadow outside the bounding box of the View
            case DragEvent.ACTION_DRAG_EXITED:
                reset_box_color (v);
                break;

            case ACTION_DROP :
                hintDisplay.setVisibility( INVISIBLE );

                if (v==box1 && textviewRandom.getText().toString().equals("H")){
                    textviewHydrogen.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.hydrogen ); }

                else if (v==box3 && textviewRandom.getText().toString().equals("He")){
                    textviewHelium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.helium ); }

                else if (v==box3 && textviewRandom.getText().toString().equals("Li")){
                    textviewLithium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.lithium ); }

                else if (v==box4 && textviewRandom.getText().equals("Be")){
                    textviewBerillium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.berillium); }

                else if (v==box5 && textviewRandom.getText().toString().equals("B")){
                    textviewBoron.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.boron ); }

                else if (v==box6 && textviewRandom.getText().toString().equals("C")){
                    textviewCarbon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.carbon ); }

                else if (v==box7 && textviewRandom.getText().toString().equals("N")){
                    textviewNitrogen.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.nitrogen ); }

                else if (v==box8 && textviewRandom.getText().toString().equals("O")){
                    textviewOxygen.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.oxygen ); }

                else if (v==box9 && textviewRandom.getText().toString().equals("F")){
                    textviewFluorine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.fluorine ); }

                else if (v==box10 && textviewRandom.getText().toString().equals("Ne")){
                    textviewNeon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.neon ); }

                else if (v==box11 && textviewRandom.getText().equals("Na")){
                    textviewSodium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.sodium); }

                else if (v==box12 && textviewRandom.getText().toString().equals("Mg")){
                    textviewMagnesium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.magnesium ); }

                else if (v==box13 && textviewRandom.getText().toString().equals("Al")){
                    textviewAluminium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.aluminium ); }

                else if (v==box14 && textviewRandom.getText().toString().equals("Si")){
                    textviewSilicon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.silicon ); }

                else if (v==box15 && textviewRandom.getText().toString().equals("P")){
                    textviewPhosphorous.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.phosphorous ); }

                else if (v==box16 && textviewRandom.getText().toString().equals("S")){
                    textviewSulfur.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.sulfur ); }

                else if (v==box17 && textviewRandom.getText().toString().equals("Cl")){
                    textviewChlorine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.chlorine ); }

                else if (v==box18 && textviewRandom.getText().toString().equals("Ar")){
                    textviewArgon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.argon ); }

                else if (v==box19 && textviewRandom.getText().equals("K")){
                    textviewPotassium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.potassium ); }

                else if (v==box20 && textviewRandom.getText().toString().equals("Ca")){
                    textviewCalcium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.calcium ); }

                else if (v==box21 && textviewRandom.getText().toString().equals("Sc")){
                    textviewScandium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.scandium ); }

                else if (v==box22 && textviewRandom.getText().toString().equals("Ti")){
                    textviewTitanium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.titanium ); }

                else if (v==box23 && textviewRandom.getText().toString().equals("V")){
                    textviewVanadium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.vanadium ); }

                else if (v==box24 && textviewRandom.getText().toString().equals("Cr")){
                    textviewChromium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.chromium ); }

                else if (v==box25 && textviewRandom.getText().toString().equals("Mn")){
                    textviewManganese.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.manganese ); }

                else if (v==box26 && textviewRandom.getText().toString().equals("Fe")){
                    textviewIron.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.iron ); }

                else if (v==box27 && textviewRandom.getText().toString().equals("Co")){
                    textviewCobalt.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.cobalt ); }

                else if (v==box28 && textviewRandom.getText().toString().equals("Ni")){
                    textviewNickel.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.nickel ); }

                else if (v==box29 && textviewRandom.getText().toString().equals("Cu")){
                    textviewCopper.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.copper ); }

                else if (v==box30 && textviewRandom.getText().toString().equals("Zn")){
                    textviewZinc.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.zinc ); }

                else if (v==box31 && textviewRandom.getText().toString().equals("Ga")){
                    textviewGallium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.gallium ); }

                else if (v==box32 && textviewRandom.getText().toString().equals("Ge")){
                    textviewGermanium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.germanium ); }

                else if (v==box33 && textviewRandom.getText().toString().equals("As")){
                    textviewArsenic.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.arsenic ); }

                else if (v==box34 && textviewRandom.getText().toString().equals("Se")){
                    textviewSelenium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.selenium ); }

                else if (v==box35 && textviewRandom.getText().toString().equals("Br")){
                    textviewBromine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.bromine ); }

                else if (v==box36 && textviewRandom.getText().toString().equals("Kr")){
                    textviewKrypton.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.krypton ); }

                else if (v==box37 && textviewRandom.getText().equals("Rb")){
                    textviewRubidium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.rubidium); }

                else if (v==box38 && textviewRandom.getText().equals("Sr")){
                    textviewStroncium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.strontium ); }

                else if (v==box39 && textviewRandom.getText().toString().equals("Y")){
                    textviewYttrium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.yttrium ); }

                else if (v==box40 && textviewRandom.getText().toString().equals("Zr")){
                    textviewZirconium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.zirconium ); }

                else if (v==box41 && textviewRandom.getText().toString().equals("Nb")){
                    textviewNiobium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.niobium ); }

                else if (v==box42 && textviewRandom.getText().toString().equals("Mo")){
                    textviewMolybdenum.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.molybdenum ); }

                else if (v==box43 && textviewRandom.getText().toString().equals("Tc")){
                    textviewTechnetium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.technetium ); }

                else if (v==box44 && textviewRandom.getText().toString().equals("Ru")){
                    textviewRuthenium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.ruthenium ); }

                else if (v==box45 && textviewRandom.getText().toString().equals("Rh")){
                    textviewRhodium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.rhodium ); }

                else if (v==box46 && textviewRandom.getText().toString().equals("Pd")){
                    textviewPalladium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.palladium ); }

                else if (v==box47 && textviewRandom.getText().toString().equals("Ag")){
                    textviewSilver.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.silver); }

                else if (v==box48 && textviewRandom.getText().toString().equals("Cd")){
                    textviewCadmium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.cadmium ); }

                else if (v==box49 && textviewRandom.getText().toString().equals("In")){
                    textviewIndium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.indium ); }

                else if (v==box50 && textviewRandom.getText().toString().equals("Sn")){
                    textviewTin.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.tin ); }

                else if (v==box51 && textviewRandom.getText().toString().equals("Sb")){
                    textviewAntimony.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.antimony ); }

                else if (v==box52 && textviewRandom.getText().toString().equals("Te")){
                    textviewTellurium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.tellurium ); }

                else if (v==box53 && textviewRandom.getText().toString().equals("I")){
                    textviewIodine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.iodine ); }

                else if (v==box54 && textviewRandom.getText().toString().equals("Xe")){
                    textviewXenon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.xenon ); }

                else if (v==box55 && textviewRandom.getText().toString().equals("Cs")){
                    textviewCaesium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.caesium ); }

                else if (v==box56 && textviewRandom.getText().toString().equals("Ba")){
                    textviewBarium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.barium ); }

                else if (v==box57 && textviewRandom.getText().toString().equals("La")){
                    textviewLanthanum.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.lanthanum ); }

                else if (v==box58 && textviewRandom.getText().toString().equals("Ce")){
                    textviewCerium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.cerium ); }

                else if (v==box59 && textviewRandom.getText().toString().equals("Pr")){
                    textviewPraseodymium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.praseodymium ); }

                else if (v==box60 && textviewRandom.getText().toString().equals("Nd")){
                    textviewNeodymium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.neodymium ); }

                else if (v==box61 && textviewRandom.getText().toString().equals("Pm")){
                    textviewPromethium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.promethium ); }

                else if (v==box62 && textviewRandom.getText().toString().equals("Sm")){
                    textviewSamarium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.samarium ); }

                else if (v==box63 && textviewRandom.getText().toString().equals("Eu")){
                    textviewEuropium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.europium ); }

                else if (v==box64 && textviewRandom.getText().toString().equals("Gd")){
                    textviewGadolinium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.gadolinium ); }

                else if (v==box65 && textviewRandom.getText().toString().equals("Tb")){
                    textviewTerbium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.terbium ); }

                else if (v==box66 && textviewRandom.getText().toString().equals("Dy")){
                    textviewDysprosium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.dysprosium ); }

                else if (v==box67 && textviewRandom.getText().toString().equals("Ho")){
                    textviewHolmium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.holmium ); }

                else if (v==box68 && textviewRandom.getText().toString().equals("Er")){
                    textviewErbium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.erbium ); }

                else if (v==box69 && textviewRandom.getText().toString().equals("Tm")){
                    textviewThulium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.thulium ); }

                else if (v==box70 && textviewRandom.getText().toString().equals("Yb")){
                    textviewYtterbium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.ytterbium ); }

                else if (v==box71 && textviewRandom.getText().toString().equals("Lu")){
                    textviewLutetium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.lutetium ); }

                else if (v==box72 && textviewRandom.getText().toString().equals("Hf")){
                    textviewHafnium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.hafnium ); }

                else if (v==box73 && textviewRandom.getText().toString().equals("Ta")){
                    textviewTantalum.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.tantalum ); }

                else if (v==box74 && textviewRandom.getText().toString().equals("W")){
                    textviewTungsten.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.tungsten ); }

                else if (v==box75 && textviewRandom.getText().toString().equals("Re")){
                    textviewRhenium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.rhenium ); }

                else if (v==box76 && textviewRandom.getText().toString().equals("Os")){
                    textviewOsmium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.osmium ); }

                else if (v==box77 && textviewRandom.getText().toString().equals("Ir")){
                    textviewIridium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.iridium ); }

                else if (v==box78 && textviewRandom.getText().toString().equals("Pt")){
                    textviewPlatinum.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.platinum ); }

                else if (v==box79 && textviewRandom.getText().toString().equals("Au")){
                    textviewGold.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.gold ); }

                else if (v==box80 && textviewRandom.getText().toString().equals("Hg")){
                    textviewMercury.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.mercury ); }

                else if (v==box81 && textviewRandom.getText().toString().equals("Tl")){
                    textviewThallium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.thallium ); }

                else if (v==box82 && textviewRandom.getText().toString().equals("Pb")){
                    textviewLead.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.lead ); }

                else if (v==box83 && textviewRandom.getText().toString().equals("Bi")){
                    textviewBismuth.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.bismuth ); }

                else if (v==box84 && textviewRandom.getText().toString().equals("Po")){
                    textviewPolonium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.polonium ); }

                else if (v==box85 && textviewRandom.getText().toString().equals("At")){
                    textviewAstatine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.astatine ); }

                else if (v==box86 && textviewRandom.getText().toString().equals("Rn")){
                    textviewRadon.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.radon ); }

                else if (v==box87 && textviewRandom.getText().toString().equals("Fr")){
                    textviewFrancium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.francium ); }

                else if (v==box88 && textviewRandom.getText().toString().equals("Ra")){
                    textviewRadium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.radium ); }

                else if (v==box89 && textviewRandom.getText().toString().equals("Ac")){
                    textviewActinium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.actinium ); }

                else if (v==box90 && textviewRandom.getText().toString().equals("Th")){
                    textviewThorium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.thorium ); }

                else if (v==box91 && textviewRandom.getText().toString().equals("Pa")){
                    textviewProtactinium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.protactinium ); }

                else if (v==box92 && textviewRandom.getText().toString().equals("U")){
                    textviewUranium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.uranium); }

                else if (v==box93 && textviewRandom.getText().toString().equals("Np")){
                    textviewNeptunium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.neptunium ); }

                else if (v==box94 && textviewRandom.getText().toString().equals("Pu")){
                    textviewPlutonium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.plutonium ); }

                else if (v==box95 && textviewRandom.getText().toString().equals("Am")){
                    textviewAmericium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.americium ); }

                else if (v==box96 && textviewRandom.getText().toString().equals("Cm")){
                    textviewCurium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.curium ); }

                else if (v==box97 && textviewRandom.getText().toString().equals("Bk")){
                    textviewBerkelium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.berkelium ); }

                else if (v==box98 && textviewRandom.getText().toString().equals("Cf")){
                    textviewCalifornium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.californium ); }

                else if (v==box99 && textviewRandom.getText().toString().equals("Es")){
                    textviewEinsteinium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.einsteinium ); }

                else if (v==box100 && textviewRandom.getText().toString().equals("Fm")){
                    textviewFermium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.fermium ); }

                else if (v==box101 && textviewRandom.getText().toString().equals("Md")){
                    textviewMendelevium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.mendelevium ); }

                else if (v==box102 && textviewRandom.getText().toString().equals("No")){
                    textviewNobelium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.nobelium ); }

                else if (v==box103 && textviewRandom.getText().toString().equals("Lr")){
                    textviewLawrencium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.lawrencium ); }

                else if (v==box104 && textviewRandom.getText().toString().equals("Rf")){
                    textviewRutherfordium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.rutherfordium ); }

                else if (v==box105 && textviewRandom.getText().toString().equals("Db")){
                    textviewDubnium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.dubnium ); }

                else if (v==box106 && textviewRandom.getText().toString().equals("Sg")){
                    textviewSeaborgium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.seaborgium ); }

                else if (v==box107 && textviewRandom.getText().toString().equals("Bh")){
                    textviewBohrium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.bohrium ); }

                else if (v==box108 && textviewRandom.getText().toString().equals("Hs")){
                    textviewHassium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.hassium ); }

                else if (v==box109 && textviewRandom.getText().toString().equals("Mt")){
                    textviewMeitnerium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.meitnerium ); }

                else if (v==box110 && textviewRandom.getText().toString().equals("Ds")){
                    textviewDarmstadtium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.darmstadtium ); }

                else if (v==box111 && textviewRandom.getText().toString().equals("Rg")){
                    textviewRoentgenium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.roentgenium ); }

                else if (v==box112 && textviewRandom.getText().toString().equals("Cn")){
                    textviewCopernicium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.copernicium ); }

                else if (v==box113 && textviewRandom.getText().toString().equals("Nh")){
                    textviewNihonium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.nihonium ); }

                else if (v==box114 && textviewRandom.getText().toString().equals("Fl")){
                    textviewFlerovium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.flerovium ); }

                else if (v==box115 && textviewRandom.getText().toString().equals("Mc")){
                    textviewMoscovium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.moscovium ); }

                else if (v==box116 && textviewRandom.getText().toString().equals("Lv")){
                    textviewLivermorium.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.livermorium ); }

                else if (v==box117 && textviewRandom.getText().toString().equals("Ts")){
                    textviewTennessine.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.tennessine ); }

                else if (v==box118 && textviewRandom.getText().toString().equals("Og")){
                    textviewOganesson.setVisibility( VISIBLE);
                    jump_animation();
                    correct_animation(v);
                    remove_el_from_db( elem, l, R.string.oganesson ); }



                else {
                    error_count++;
                    error.setText( "" + (error_count) );
                    place_animation_V( v );}
                break; }
        return true;

    }


//********************* SUPPORTING METHODS****************************************



    //Most important: defines the level layout
    protected void add_element_for_level (Element e) {
        switch (level_type) {

            case "Group":
                if (getResources().getString(e.getType()).equalsIgnoreCase( s ) ) {
                    l.add( e );
                    e.getTextView().setVisibility( INVISIBLE );
                    llBoxlan.setBackgroundResource( R.drawable.box2_unknown );
                    llBoxact.setBackgroundResource( R.drawable.box2_unknown );
                } else{
                    e.getPosition().setBackgroundResource( R.drawable.box2_unknown );
                    e.getPosition().setOnDragListener( null );}
                break;

            case "Element_list":
                if (s.equals( "Elements_1_36" )) {
                    if (e.getPosition() == box1 || e.getPosition() == box2 || e.getPosition() == box3 || e.getPosition() == box4 ||
                            e.getPosition() == box5 || e.getPosition() == box6 || e.getPosition() == box7 || e.getPosition() == box8 ||
                            e.getPosition() == box9 || e.getPosition() == box10 || e.getPosition() == box11 || e.getPosition() == box12 ||
                            e.getPosition() == box13 || e.getPosition() == box14 || e.getPosition() == box15 || e.getPosition() == box16 ||
                            e.getPosition() == box17 || e.getPosition() == box18 || e.getPosition() == box19 || e.getPosition() == box20 ||
                            e.getPosition() == box21 || e.getPosition() == box22 || e.getPosition() == box23 || e.getPosition() == box24 ||
                            e.getPosition() == box25 || e.getPosition() == box26 || e.getPosition() == box27 || e.getPosition() == box28 ||
                            e.getPosition() == box29 || e.getPosition() == box30 || e.getPosition() == box31 || e.getPosition() == box32 ||
                            e.getPosition() == box33 || e.getPosition() == box34 || e.getPosition() == box35 || e.getPosition() == box36 ) {
                        l.add( e );
                        e.getTextView().setVisibility( INVISIBLE );
                        llBoxlan.setBackgroundResource( R.drawable.box2_unknown );
                        llBoxact.setBackgroundResource( R.drawable.box2_unknown );
                    } else
                    {e.getPosition().setBackgroundResource( R.drawable.box2_unknown );
                        e.getPosition().setOnDragListener( null );}
                }
                if (s.equals( "Elements_1_56" )) {
                    if (e.getPosition() == box1 || e.getPosition() == box2 || e.getPosition() == box3 || e.getPosition() == box4 ||
                            e.getPosition() == box5 || e.getPosition() == box6 || e.getPosition() == box7 || e.getPosition() == box8 ||
                            e.getPosition() == box9 || e.getPosition() == box10 || e.getPosition() == box11 || e.getPosition() == box12 ||
                            e.getPosition() == box13 || e.getPosition() == box14 || e.getPosition() == box15 || e.getPosition() == box16 ||
                            e.getPosition() == box17 || e.getPosition() == box18 || e.getPosition() == box19 || e.getPosition() == box20 ||
                            e.getPosition() == box21 || e.getPosition() == box22 || e.getPosition() == box23 || e.getPosition() == box24 ||
                            e.getPosition() == box25 || e.getPosition() == box26 || e.getPosition() == box27 || e.getPosition() == box28 ||
                            e.getPosition() == box29 || e.getPosition() == box30 || e.getPosition() == box31 || e.getPosition() == box32 ||
                            e.getPosition() == box33 || e.getPosition() == box34 || e.getPosition() == box35 || e.getPosition() == box36 ||
                            e.getPosition() == box37 || e.getPosition() == box38 || e.getPosition() == box39 || e.getPosition() == box40 ||
                            e.getPosition() == box41 || e.getPosition() == box42 || e.getPosition() == box43 || e.getPosition() == box44 ||
                            e.getPosition() == box45 || e.getPosition() == box46 || e.getPosition() == box47 || e.getPosition() == box48 ||
                            e.getPosition() == box49 || e.getPosition() == box50 || e.getPosition() == box51 || e.getPosition() == box52 ||
                            e.getPosition() == box53 || e.getPosition() == box54 || e.getPosition() == box55 || e.getPosition() == box56) {
                        l.add( e );
                        e.getTextView().setVisibility( INVISIBLE );
                        llBoxlan.setBackgroundResource( R.drawable.box2_unknown );
                        llBoxact.setBackgroundResource( R.drawable.box2_unknown );
                    } else
                    {e.getPosition().setBackgroundResource( R.drawable.box2_unknown );
                        e.getPosition().setOnDragListener( null );}
                } break; }
    }

    protected void add_random_for_level (ArrayList <Element> k){
        switch (s) {
            case "Random1":
                while (k.size()>35) {
                    int n = new Random().nextInt(k.size());
                    Element e= k.get( n );
                    e.getPosition().setOnDragListener( null );
                    e.getTextView().setVisibility( VISIBLE );
                    k.remove(e);
                } break;
            case "Random2":
                while (k.size()>71) {
                    int n = new Random().nextInt(k.size());
                    Element e= k.get( n );
                    e.getPosition().setOnDragListener( null );
                    e.getTextView().setVisibility( VISIBLE );
                    k.remove(e);
                } break;
        } }


    // Remove the element that was placed and set the new one
    protected void remove_el_from_db (Element e, ArrayList<Element> arl, int symbol){
        if (arl.size()>1){
            for (int i=0; i<arl.size(); i++) {
                e = ((Element)arl.get(i));
                if (e.getSymbol()==symbol)  {
                    e.getPosition().setOnDragListener( null );
                    arl.remove( e );
                    Element rand_element = arl.get( new Random().nextInt(arl.size()));
                    textviewRandom.setText(rand_element.getSymbol());
                    randomInfoDisplayText.setText(rand_element.getRandomInfo());
                    hintDisplayText.setText( rand_element.getHint() );
                    if (t != null) { t.cancel(); }
                    startTimer( 20000, 1000 ); } }}
        else {
            box0.setVisibility( INVISIBLE );
            t.cancel();
            time.setText( "00" );
            opaqueLayer.setVisibility( VISIBLE );
            finalscreenDisplay.setVisibility( VISIBLE );
            finalscreenDisplay.bringToFront();
            errorFinaldisplay.setText( "Errors: " + (error_count) );
            hintButton.setClickable( false ); } }

    //Timer

    public void startTimer(final long finish, long tick) {

        t = new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                time.setText("" + (remainedSecs % 60)); }

            public void onFinish() {
                time.setText("00");
                t.cancel();
                placeButton.performClick();
            }}.start(); }

//****************************************************************************************************
    //   Animations and view definition methods     **************************************************

    public void jump_animation(){
        chemist.setBackgroundResource(R.drawable.jump_list);
        AnimationDrawable shAnimation = (AnimationDrawable) chemist.getBackground();
        shAnimation.start();
        shAnimation.setOneShot( true ); }

    public void read_animation(){
        chemist.setBackgroundResource(R.drawable.livre_list);
        AnimationDrawable livAnimation = (AnimationDrawable) chemist.getBackground();
        livAnimation.start(); }

    public void cancel_animation_and_hint(){
        chemist.clearAnimation();
        chemist.setBackgroundResource( R.drawable.jumo1 );
        hintDisplay.setVisibility( INVISIBLE ); }

    public void floating_box() {
        ObjectAnimator animation = ObjectAnimator.ofFloat( box0, "translationY", 10f );
        animation.setDuration( 1000 );
        ObjectAnimator animation2 = ObjectAnimator.ofFloat( box0, "translationY", 0f );
        animation2.setDuration( 1000 );
        final AnimatorSet as = new AnimatorSet();
        as.playSequentially( animation, animation2 );
        as.addListener( new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                as.start(); }
        } );as.start(); }

    public void reset_box_color(View v) {
        if (v == box1) {
            v.setBackgroundResource( R.drawable.box2_hydrogen ); }
        else if (v == box2 || v == box10 || v == box18 || v == box36 || v == box54 || v == box86) {
            v.setBackgroundResource( R.drawable.box2_noblegases ); }
        else if (v == box3 || v == box11 || v == box19 || v == box37 || v == box55 || v == box87) {
            v.setBackgroundResource( R.drawable.box2_alkaline ); }
        else if (v == box4 || v == box12 || v == box20 || v == box38 || v == box56 || v == box88) {
            v.setBackgroundResource( R.drawable.box2_alkalineearth ); }
        else if (v == box21 || v == box22 || v == box23 || v == box24 || v == box25 || v == box26 ||
                v == box27 || v == box28 || v == box29 || v == box30 || v == box39 || v == box40 || v == box41 ||
                v == box42 || v == box43 || v == box44 || v == box45 || v == box46 || v == box47 || v == box48
                || v == box72 || v == box73 || v == box74 || v == box75 || v == box76 || v == box77 || v == box78
                || v == box79 || v == box80 || v == box104 || v == box105 || v == box106 || v == box107 || v == box108) {
            v.setBackgroundResource( R.drawable.box2_tmetals ); }
        else if (v == box13 || v == box31 || v == box49 || v == box50 || v == box81 || v == box82 || v == box83) {
            v.setBackgroundResource( R.drawable.box2_posttm ); }
        else if (v == box5 || v == box14 || v == box32 || v == box33 || v == box51 || v == box52 || v == box84) {
            v.setBackgroundResource( R.drawable.box2_metalloids ); }
        else if (v == box6 || v == box7 || v == box8 || v == box15 || v == box16 || v == box34) {
            v.setBackgroundResource( R.drawable.box2_nonmetals ); }
        else if (v == box9 || v == box17 || v == box35 || v == box53 || v == box85) {
            v.setBackgroundResource( R.drawable.box2_halogens ); }
        else if (v == box109 || v == box110 || v == box111 || v == box112 || v == box113 || v == box114 || v == box115
                || v == box116 || v == box117 || v == box118) {
            v.setBackgroundResource( R.drawable.box2_unknown ); }
        else if (v == box57 || v == box58 || v == box59 || v == box60 || v == box61 || v == box62 || v == box63
                || v == box64 || v == box65 || v == box66 || v == box67 || v == box68 || v == box69 || v == box70 || v == box71) {
            v.setBackgroundResource( R.drawable.box2_lanthanides ); }
        else if (v == box89 || v == box90 || v == box91 || v == box92 || v == box93 || v == box94 || v == box95
                || v == box96 || v == box97 || v == box98 || v == box99 || v == box100 || v == box101 || v == box102 || v == box103) {
            v.setBackgroundResource( R.drawable.box2_actinides ); }
        else {v.setBackgroundResource( R.drawable.box2 );}
    }

    public void place_animation (int symbol) {
        for (int i=0; i<l.size(); i++) {
            Element e = l.get(i);
            if (e.getSymbol()==symbol){
                View v = e.getPosition();
                reset_box_color( v );
                if (v==box1) {
                    v.setBackgroundResource(R.drawable.place_list_hydrogen ); }
                if (v==box2 || v==box10 || v==box18|| v==box36 || v==box54 || v==box86){
                    v.setBackgroundResource(R.drawable.place_list_noblegases ); }
                if (v==box3 || v==box11 || v==box19|| v==box37 || v==box55 || v==box87){
                    v.setBackgroundResource(R.drawable.place_list_alkaline ); }
                if (v==box4 || v==box12 || v==box20|| v==box38 || v==box56 || v==box88){
                    v.setBackgroundResource(R.drawable.place_list_alkalineearth ); }
                if (v==box21 || v==box22 || v==box23|| v==box24 || v==box25 || v==box26 ||
                        v==box27 || v==box28 || v==box29|| v==box30 || v==box39 || v==box40|| v==box41 ||
                        v==box42 || v==box43 || v==box44|| v==box45 || v==box46 || v==box47|| v==box48
                        || v==box72 || v==box73 || v==box74|| v==box75 || v==box76 || v==box77|| v==box78
                        || v==box79 || v==box80 || v==box104|| v==box105 || v==box106 || v==box107|| v==box108){
                    v.setBackgroundResource(R.drawable.place_list_tmetals ); }
                if (v==box13 || v==box31 || v==box49|| v==box50 || v==box81 || v==box82|| v==box83){
                    v.setBackgroundResource(R.drawable.place_list_posttm ); }
                if (v==box5 || v==box14 || v==box32|| v==box33 || v==box51 || v==box52|| v==box84){
                    v.setBackgroundResource(R.drawable.place_list_metalloids ); }
                if (v==box6 || v==box7 || v==box8|| v==box15 || v==box16 || v==box34){
                    v.setBackgroundResource(R.drawable.place_list_nonmetals ); }
                if (v==box9 || v==box17 || v==box35|| v==box53 || v==box85 ){
                    v.setBackgroundResource(R.drawable.place_list_halogens ); }
                if (v==box109 || v==box110 || v==box111|| v==box112 || v==box113 || v==box114|| v==box115
                        || v==box116 || v==box117 || v==box118){
                    v.setBackgroundResource(R.drawable.place_list_unknown ); }
                if (v==box57 || v==box58 || v==box59|| v==box60 || v==box61 || v==box62|| v==box63
                        || v==box64 || v==box65 || v==box66 || v==box67 || v==box68 || v==box69|| v==box70 || v==box71){
                    v.setBackgroundResource(R.drawable.place_list_lanthanides ); }
                if (v==box89 || v==box90 || v==box91|| v==box92 || v==box93 || v==box94|| v==box95
                        || v==box96 || v==box97 || v==box98 || v==box99 || v==box100 || v==box101|| v==box102 || v==box103){
                    v.setBackgroundResource(R.drawable.place_list_actinides );
                }
                AnimationDrawable placeAnimation = (AnimationDrawable) v.getBackground();
                placeAnimation.start(); }}
    }

    public void place_animation_V (View v) {
        reset_box_color( v );
        if (v==box1) {
            v.setBackgroundResource(R.drawable.place_list_hydrogen ); }
        if (v==box2 || v==box10 || v==box18|| v==box36 || v==box54 || v==box86){
            v.setBackgroundResource(R.drawable.place_list_noblegases ); }
        if (v==box3 || v==box11 || v==box19|| v==box37 || v==box55 || v==box87){
            v.setBackgroundResource(R.drawable.place_list_alkaline ); }
        if (v==box4 || v==box12 || v==box20|| v==box38 || v==box56 || v==box88){
            v.setBackgroundResource(R.drawable.place_list_alkalineearth ); }
        if (v==box21 || v==box22 || v==box23|| v==box24 || v==box25 || v==box26 ||
                v==box27 || v==box28 || v==box29|| v==box30 || v==box39 || v==box40|| v==box41 ||
                v==box42 || v==box43 || v==box44|| v==box45 || v==box46 || v==box47|| v==box48
                || v==box72 || v==box73 || v==box74|| v==box75 || v==box76 || v==box77|| v==box78
                || v==box79 || v==box80 || v==box104|| v==box105 || v==box106 || v==box107|| v==box108){
            v.setBackgroundResource(R.drawable.place_list_tmetals ); }
        if (v==box13 || v==box31 || v==box49|| v==box50 || v==box81 || v==box82|| v==box83){
            v.setBackgroundResource(R.drawable.place_list_posttm ); }
        if (v==box5 || v==box14 || v==box32|| v==box33 || v==box51 || v==box52|| v==box84){
            v.setBackgroundResource(R.drawable.place_list_metalloids ); }
        if (v==box6 || v==box7 || v==box8|| v==box15 || v==box16 || v==box34){
            v.setBackgroundResource(R.drawable.place_list_nonmetals ); }
        if (v==box9 || v==box17 || v==box35|| v==box53 || v==box85 ){
            v.setBackgroundResource(R.drawable.place_list_halogens ); }
        if (v==box109 || v==box110 || v==box111|| v==box112 || v==box113 || v==box114|| v==box115
                || v==box116 || v==box117 || v==box118){
            v.setBackgroundResource(R.drawable.place_list_unknown ); }
        if (v==box57 || v==box58 || v==box59|| v==box60 || v==box61 || v==box62|| v==box63
                || v==box64 || v==box65 || v==box66 || v==box67 || v==box68 || v==box69|| v==box70 || v==box71){
            v.setBackgroundResource(R.drawable.place_list_lanthanides ); }
        if (v==box89 || v==box90 || v==box91|| v==box92 || v==box93 || v==box94|| v==box95
                || v==box96 || v==box97 || v==box98 || v==box99 || v==box100 || v==box101|| v==box102 || v==box103){
            v.setBackgroundResource(R.drawable.place_list_actinides );
        }
        AnimationDrawable placeAnimation = (AnimationDrawable) v.getBackground();
        placeAnimation.start(); }

    public void correct_animation (View v) {
        reset_box_color( v );
        if (v==box1) {
            v.setBackgroundResource(R.drawable.list_correct_hydrogen ); }
        if (v==box2 || v==box10 || v==box18|| v==box36 || v==box54 || v==box86){
            v.setBackgroundResource(R.drawable.list_correct_noblegases ); }
        if (v==box3 || v==box11 || v==box19|| v==box37 || v==box55 || v==box87){
            v.setBackgroundResource(R.drawable.list_correct_alkaline ); }
        if (v==box4 || v==box12 || v==box20|| v==box38 || v==box56 || v==box88){
            v.setBackgroundResource(R.drawable.list_correct_alkearth ); }
        if (v==box21 || v==box22 || v==box23|| v==box24 || v==box25 || v==box26 ||
                v==box27 || v==box28 || v==box29|| v==box30 || v==box39 || v==box40|| v==box41 ||
                v==box42 || v==box43 || v==box44|| v==box45 || v==box46 || v==box47|| v==box48
                || v==box72 || v==box73 || v==box74|| v==box75 || v==box76 || v==box77|| v==box78
                || v==box79 || v==box80 || v==box104|| v==box105 || v==box106 || v==box107|| v==box108){
            v.setBackgroundResource(R.drawable.list_correct_tmetals ); }
        if (v==box13 || v==box31 || v==box49|| v==box50 || v==box81 || v==box82|| v==box83){
            v.setBackgroundResource(R.drawable.list_correct_posttm ); }
        if (v==box5 || v==box14 || v==box32|| v==box33 || v==box51 || v==box52|| v==box84){
            v.setBackgroundResource(R.drawable.list_correct_metalloids ); }
        if (v==box6 || v==box7 || v==box8|| v==box15 || v==box16 || v==box34){
            v.setBackgroundResource(R.drawable.list_correct_nonmetals ); }
        if (v==box9 || v==box17 || v==box35|| v==box53 || v==box85 ){
            v.setBackgroundResource(R.drawable.list_correct_halogens ); }
        if (v==box109 || v==box110 || v==box111|| v==box112 || v==box113 || v==box114|| v==box115
                || v==box116 || v==box117 || v==box118){
            v.setBackgroundResource(R.drawable.list_correct_unknown ); }
        if (v==box57 || v==box58 || v==box59|| v==box60 || v==box61 || v==box62|| v==box63
                || v==box64 || v==box65 || v==box66 || v==box67 || v==box68 || v==box69|| v==box70 || v==box71){
            v.setBackgroundResource(R.drawable.list_correct_lanthanides ); }
        if (v==box89 || v==box90 || v==box91|| v==box92 || v==box93 || v==box94|| v==box95
                || v==box96 || v==box97 || v==box98 || v==box99 || v==box100 || v==box101|| v==box102 || v==box103){
            v.setBackgroundResource(R.drawable.list_correct_actinides );
        }
        AnimationDrawable correctAnimation = (AnimationDrawable) v.getBackground();
        correctAnimation.start();
    }

    //**********************************************************************************************************
    //         Activity lifecycle methods          *********************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(t != null) {
            t.cancel();
            t = null; }
        this.finish(); }

    @Override
    protected void onRestart() {
        super.onRestart();
        recreate(); }
}




